
#include "appconfig.h"
#include "filesystem.h"
#include "misc.hpp"

#include "include/rapidjson/istreamwrapper.h"

#include <fstream>

#define _LD_AC_CASE_GENERAL_INT64(name) \
    case Misc::fnvHashConst(#name): \
        assert(value.IsBool() || value.IsInt() || value.IsUint() || value.IsInt64()); \
        if (value.IsInt64()) \
            mConfig->Get()->name = value.GetInt64(); \
        else if (value.IsUint()) \
            mConfig->Get()->name = value.GetUint(); \
        else if (value.IsInt()) \
            mConfig->Get()->name = value.GetInt(); \
        else if (value.IsBool()) { \
            gLogger->TraceWarn("Converting value from bool to int64"); \
            mConfig->Get()->name = value.GetBool() ? 1 : 0; \
        } \
        return true

#define _LD_AC_CASE_USCD_INT64(name) \
    case Misc::fnvHashConst(Misc::const_replace(#name, '_', '-')): \
        assert(value.IsBool() || value.IsInt() || value.IsUint() || value.IsInt64()); \
        if (value.IsInt64()) \
            mConfig->Get()->name = value.GetInt64(); \
        else if (value.IsUint()) \
            mConfig->Get()->name = value.GetUint(); \
        else if (value.IsInt()) \
            mConfig->Get()->name = value.GetInt(); \
        else if (value.IsBool()) { \
            gLogger->TraceWarn("Converting value from bool to int64"); \
            mConfig->Get()->name = value.GetBool() ? 1 : 0; \
        } \
        return true

#define _LD_AC_CASE_GENERAL_BOOL(name) \
    case Misc::fnvHashConst(#name): \
        assert(value.IsBool() || value.IsNumber()); \
        mConfig->Get()->name = (value.IsBool() && value.GetBool()) || \
            (value.IsNumber() && value.GetDouble() != 0); \
        return true

#define _LD_AC_CASE_GENERAL_STRING(name) \
    case Misc::fnvHashConst(#name): \
        assert(value.IsString()); \
        mConfig->Get()->name = Misc::fnvHash(value.GetString()); \
        return true

#define _LD_AC_CHECK_MUTEX(func) \
        if (mConfigMutex->TryLock()) { \
            gLogger->TraceError("AppConfig mutex was not locked before calling " func " - FATAL"); \
            abort(); \
        }

namespace LeagueDisplays
{
    /* static */ Logging::LoggerPtr AppConfig::gLogger;
    /* static */ AppConfig* AppConfig::mInstance = nullptr;

    AppConfig::AppConfig()
    {
        gLogger = Logging::LoggerManager::GetLogger("AppConfig");
        mConfigMutex = new CrossProcessMutex("/LeagueDisplays_AppConfigMutex");

        mBackgroundPlaylistChanged = false;
        mScreensaverPlaylistChanged = false;
        mConfig = new CrossProcessObject<ld_settings_t>("/LeagueDisplays_Config");
    }

    /* static */ AppConfig::AcquiredAppConfig AppConfig::Acquire()
    {
        if (!mInstance)
            mInstance = new AppConfig();

        mInstance->mConfigMutex->Lock();
        return mInstance;
    }

    void AppConfig::Release()
    {
        _LD_AC_CHECK_MUTEX("AppConfig::Release")
        mConfigMutex->Unlock();
    }

    void AppConfig::LoadSettings()
    {
        _LD_AC_CHECK_MUTEX("AppConfig::LoadSettings")

        mConfig->Get()->mReloadConfig = true;

        gLogger->TraceDebug("Loading settings from %s", Filesystem::gSettingsFilePath.c_str());

        std::ifstream ifs(Filesystem::gSettingsFilePath);
        assert(ifs.is_open());
        rapidjson::IStreamWrapper wrapper(ifs);

        rapidjson::Document d;
        d.ParseStream(wrapper);
        ifs.close();

        for (auto iter = d.MemberBegin(); iter != d.MemberEnd(); ++iter)
            ModifyOption(std::string(iter->name.GetString()), iter->value, ConfigType::GENERAL);
    }

    void AppConfig::LoadPlaylist()
    {
        _LD_AC_CHECK_MUTEX("AppConfig::LoadPlaylist")

        mConfig->Get()->mReloadPlaylist = true;

        gLogger->TraceDebug("Loading playlist from %s", Filesystem::gPlaylistsFilePath.c_str());

        std::ifstream ifs(Filesystem::gPlaylistsFilePath);
        assert(ifs.is_open());
        rapidjson::IStreamWrapper wrapper(ifs);

        rapidjson::Document d;
        d.ParseStream(wrapper);
        ifs.close();

        if (d.HasMember("wallpaper"))
        {
            auto& wallpaper_obj = d["wallpaper"];

            if (wallpaper_obj.HasMember("assets"))
            {
                auto& wallpaper_assets_list = wallpaper_obj["assets"];

                if (wallpaper_assets_list.IsArray())
                {
                    mBackgroundPlaylist.clear();
                    mBackgroundPlaylistChanged = true;

                    for (size_t i = 0; i < wallpaper_assets_list.Size(); i++)
                    {
                        if (!wallpaper_assets_list[i].IsString())
                            continue;

                        mBackgroundPlaylist.push_back(wallpaper_assets_list[i].GetString());
                    }
                }
            }
        }

        if (d.HasMember("screensaver"))
        {
            auto& screensaver_obj = d["screensaver"];

            if (screensaver_obj.HasMember("assets"))
            {
                auto& screensaver_assets_list = screensaver_obj["assets"];

                if (screensaver_assets_list.IsArray())
                {
                    mScreensaverPlaylist.clear();
                    mScreensaverPlaylistChanged = true;

                    for (size_t i = 0; i < screensaver_assets_list.Size(); i++)
                    {
                        if (!screensaver_assets_list[i].IsString())
                            continue;

                        mScreensaverPlaylist.push_back(screensaver_assets_list[i].GetString());
                    }
                }
            }
        }

        gLogger->TraceDebug("There are %d wallpapers and %d screensavers after loading playlist",
            mBackgroundPlaylist.size(), mScreensaverPlaylist.size());
    }

    bool AppConfig::ModifyOption(const std::string& fnv_name, const rapidjson::Value& value, const ConfigType type)
    {
        gLogger->TraceDebug("ModifyOption(%s)", fnv_name.c_str());
        return AppConfig::ModifyOption(Misc::fnvHash(fnv_name.c_str()), value, type);
    }

    bool AppConfig::ModifyOption(const unsigned int fnv_name, const rapidjson::Value& value, const ConfigType type)
    {
        _LD_AC_CHECK_MUTEX("AppConfig::ModifyOption")

        if (type == ConfigType::GENERAL)
        {
            switch (fnv_name)
            {
                _LD_AC_CASE_GENERAL_INT64  ( firstTimeConfig                );
                _LD_AC_CASE_GENERAL_INT64  ( lifetimeSessions               );
                _LD_AC_CASE_GENERAL_INT64  ( lifetimeSaves                  );
                _LD_AC_CASE_GENERAL_INT64  ( wp_imageDuration               );
                _LD_AC_CASE_GENERAL_INT64  ( ss_videoDuration               );
                _LD_AC_CASE_GENERAL_BOOL   ( ss_mirrorDisplay               );
                _LD_AC_CASE_GENERAL_INT64  ( agreeLicenseTimestamp          );
                _LD_AC_CASE_GENERAL_INT64  ( openConfigTimestamp            );
                _LD_AC_CASE_GENERAL_INT64  ( newestContentTimestamp         );
                _LD_AC_CASE_GENERAL_STRING ( locale                         );
                _LD_AC_CASE_GENERAL_BOOL   ( crop_images                    );
                _LD_AC_CASE_GENERAL_INT64  ( savePlaylistTimestamp          );

                default:
                    gLogger->TraceError("Not handled option in group 'GENERAL': hash=0x%08x", fnv_name);
                    return false;
            }
        }
        else if (type == ConfigType::WALLPAPER)
        {
            switch (fnv_name)
            {
                _LD_AC_CASE_USCD_INT64     ( wallpaper_rotate_frequency     );

                default:
                    gLogger->TraceError("Not handled option in group 'WALLPAPER': hash=0x%08x", fnv_name);
                    return false;
            }
        }
        else if (type == ConfigType::SCREENSAVER)
        {
            switch (fnv_name)
            {
                _LD_AC_CASE_USCD_INT64     ( screensaver_timeout            );

                default:
                    gLogger->TraceError("Not handled option in group 'SCREENSAVER': hash=0x%08x", fnv_name);
                    return false;
            }
        }
        else
        {
            gLogger->TraceError("Invalid option group - FATAL");
            abort();
            return false;
        }
    }

    void AppConfig::ReloadIfRequested()
    {
        if (mConfig->Get()->mReloadConfig)
        {
            LoadSettings();
            mConfig->Get()->mReloadConfig = false;
        }

        if (mConfig->Get()->mReloadPlaylist)
        {
            LoadPlaylist();
            mConfig->Get()->mReloadPlaylist = false;
        }
    }

    std::vector<std::string>& AppConfig::BackgroundPlaylist()
    {
        return mBackgroundPlaylist;
    }

    std::vector<std::string>& AppConfig::ScreensaverPlaylist()
    {
        return mScreensaverPlaylist;
    }

    ld_settings_t *AppConfig::Settings()
    {
        if (mConfig->Get()->mReloadConfig)
        {
            mConfig->Get()->mReloadConfig = false;
            LoadSettings();
        }

        return this->mConfig->Get();
    }

    bool AppConfig::BackgroundPlaylistChanged()
    {
        if (mConfig->Get()->mReloadPlaylist)
        {
            mConfig->Get()->mReloadPlaylist = false;
            LoadPlaylist();
        }

        return this->mBackgroundPlaylistChanged ? !(this->mBackgroundPlaylistChanged = false) : false;
    }

    bool AppConfig::ScreensaverPlaylistChanged()
    {
        if (mConfig->Get()->mReloadPlaylist)
        {
            mConfig->Get()->mReloadPlaylist = false;
            LoadPlaylist();
        }

        return this->mScreensaverPlaylistChanged ? !(this->mScreensaverPlaylistChanged = false) : false;
    }
}
