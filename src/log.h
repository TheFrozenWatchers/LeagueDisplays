
#ifndef LOG_H
#define LOG_H

#include <string>
#include <map>
#include <memory>
#include <iostream>
#include <deque>
#include <mutex>
#include <thread>

namespace LeagueDisplays
{
    namespace Logging
    {
        enum LogLevel {
            LL_DEBUG,
            LL_INFO,
            LL_FIXME,
            LL_WARN,
            LL_ERROR,
            LL_DEFAULT = LL_INFO
        };

        class LoggerManager;

        class LogOutputWriter
        {
            public:
                using streamptr = std::unique_ptr<std::ostream>;

                LogOutputWriter(std::ostream& console, streamptr file);
                ~LogOutputWriter();

                void Output(std::string what);
                void Flush();
                void FlushQueue();

                void GoAsync() noexcept;
            private:
                bool Poll();
                static void OutputWithFormatting(std::ostream& os, std::string input);
                static void OutputWithoutFormatting(std::ostream& os, std::string input);

                std::ostream                &mConsoleOutput;
                streamptr                    mFileOutput;

                std::deque<std::string>      mLogQueue;

                std::mutex                   mQueueMutex;
                std::mutex                   mOutputMutex;
                std::unique_ptr<std::thread> mPollingThread;

                bool                         mRunningAsync;
                bool                         mRun;
        };

        template<typename ... Args>
        std::string FormatString(std::string format, Args... args)
        {
            int size = snprintf(nullptr, 0, format.c_str(), args...);

            if (size < 0) throw std::runtime_error{"String format error"};

            size++; // string terminator \0

            std::unique_ptr<char[]> buf{new char[size]};
            snprintf(buf.get(), size, format.c_str(), args...);

            return std::string{buf.get(), buf.get() + size - 1};
        }

        class Logger
        {
            public:
                Logger(std::shared_ptr<LogOutputWriter> writer, std::string name);

                virtual ~Logger() = default;

                template<typename ... Args>
                void DebugTr(const char *trace, std::string format, Args... args) {
                    LogRaw(trace, LogLevel::LL_DEBUG, format, args...);
                }

                template<typename ... Args>
                void Debug(std::string format, Args... args) {
                    LogRaw(nullptr, LogLevel::LL_DEBUG, format, args...);
                }


                template<typename ... Args>
                void InfoTr(const char *trace, std::string format, Args... args) {
                    LogRaw(trace, LogLevel::LL_INFO, format, args...);
                }

                template<typename ... Args>
                void Info(std::string format, Args... args) {
                    LogRaw(nullptr, LogLevel::LL_INFO, format, args...);
                }


                template<typename ... Args>
                void FixmeTr(const char *trace, std::string format, Args... args) {
                    LogRaw(trace, LogLevel::LL_FIXME, format, args...);
                }

                template<typename ... Args>
                void Fixme(std::string format, Args... args) {
                    LogRaw(nullptr, LogLevel::LL_FIXME, format, args...);
                }


                template<typename ... Args>
                void WarnTr(const char *trace, std::string format, Args... args) {
                    LogRaw(trace, LogLevel::LL_WARN, format, args...);
                }

                template<typename ... Args>
                void Warn(std::string format, Args... args) {
                    LogRaw(nullptr, LogLevel::LL_WARN, format, args...);
                }


                template<typename ... Args>
                void ErrorTr(const char *trace, std::string format, Args... args) {
                    LogRaw(trace, LogLevel::LL_ERROR, format, args...);
                }

                template<typename ... Args>
                void Error(std::string format, Args... args) {
                    LogRaw(nullptr, LogLevel::LL_ERROR, format, args...);
                }


                template<typename ... Args>
                void Fatal(std::string format, Args... args) {
                    LogRaw(nullptr, LogLevel::LL_ERROR, format, args...);
                    LogRaw(nullptr, LogLevel::LL_ERROR, "Terminating application");
                    mWriter->FlushQueue();
                    abort();
                }


                template<typename ... Args>
                void LogRaw(const char *trace, LogLevel level, std::string format, Args... args) {
                    LogRaw(trace, level, FormatString(format, args...));
                }

                virtual void LogRaw(const char *trace, LogLevel level, std::string message);
                virtual void LogOutputRaw(LogLevel level, std::string message);

                std::string                            mName;
            private:
                std::shared_ptr<LogOutputWriter>       mWriter;
        };

        struct DummyLogger : public Logger
        {
            DummyLogger() : Logger{nullptr, ""} {}
            virtual void LogRaw(const char *trace, LogLevel level, std::string message) override {}
            virtual void LogOutputRaw(LogLevel level, std::string message) override {}
        };

        using LoggerPtr = std::shared_ptr<Logger>;

        class LoggerManager final
        {
            public:
                LogLevel GetMinimumLogLevel() const noexcept;
                void SetMinimumLogLevel(LogLevel level) noexcept;
                bool IsLevelEnabled(LogLevel level) const noexcept;
                void EnableAsyncLogging() noexcept;

                static void Initialize(bool log2file);
                static std::shared_ptr<Logger> GetLogger(std::string name);
                static LoggerManager& Instance() noexcept { return *gInstance; }
            private:
                LoggerManager(bool log2file);
                LoggerManager(const LoggerManager&) = delete;
                LoggerManager& operator=(const LoggerManager&) = delete;

                static std::unique_ptr<LoggerManager>           gInstance;
                static std::shared_ptr<DummyLogger>             gDummyLogger;

                std::map<std::string, std::shared_ptr<Logger>>  mLoggers;
                std::mutex                                      mLoggersMutex;
                std::shared_ptr<LogOutputWriter>                mGlobalLogWriter;

                LogLevel                                        mMinimumLevel;
        };

        // Tiny superclass that makes the code cleaner
        struct LoggerHolder {
            LoggerHolder(std::string name) : mLogger{LoggerManager::GetLogger(std::move(name))} { }
            LoggerPtr mLogger;
        };
    }
}

#define TraceDebug(...) DebugTr(__FUNCTION__, __VA_ARGS__)
#define TraceInfo(...)  InfoTr(__FUNCTION__, __VA_ARGS__)
#define TraceFixme(...) FixmeTr(__FUNCTION__, __VA_ARGS__)
#define TraceWarn(...)  WarnTr(__FUNCTION__, __VA_ARGS__)
#define TraceError(...) ErrorTr(__FUNCTION__, __VA_ARGS__)

#endif // LOG_H
