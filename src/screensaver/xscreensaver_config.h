
#ifndef LD_XSCREENSAVER_CONFIG_H
#define LD_XSCREENSAVER_CONFIG_H

#include "filesystem.h"

#include <string>
#include <map>
#include <vector>

namespace LeagueDisplays
{
    struct XScreensaverProgram
    {
        bool enabled;
        std::string mode;
        std::string name;
        stdfs::path exe;
        std::string commandline;
    };

    class XScreensaverConfig
    {
        public:
            XScreensaverConfig(stdfs::path file);
            void Reload();
            void Save();
            void SaveAs(stdfs::path file);

            bool HasKey(std::string name);
            std::string GetKeyValue(std::string name);
            void SetKeyValue(std::string name, std::string value);

            bool ScreensaverExists(std::string name);
            std::string GetScreensaverCommandLine(std::string name);
            void AddOrModifyScreensaver(std::string name, std::string commandline, std::string mode, bool enabled);

        private:
            stdfs::path                             mFilePath;
            std::map<std::string, std::string>      mConfig;
            std::vector<XScreensaverProgram>        mPrograms;
    };
}

#endif
