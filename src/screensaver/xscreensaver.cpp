
#include <string>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>

#include "ui/uibase.h"
#include "log.h"
#include "filesystem.h"
#include "xscreensaver_config.h"
#include "xscreensaver.h"

#include <sys/stat.h>

#include "include/cef_app.h"
#include "include/cef_client.h"
#include "include/cef_render_handler.h"
#include "include/cef_life_span_handler.h"
#include "include/cef_load_handler.h"
#include "include/wrapper/cef_helpers.h"

#include "vroot.h"

namespace LeagueDisplays
{
    namespace UI
    {
        int XSSErrorHandlerImpl(Display* display, XErrorEvent* event)
        {
            Display* dpy = XOpenDisplay(nullptr);
            char errormsg[128];
            XGetErrorText(dpy, event->error_code, errormsg, 127);
            XCloseDisplay(dpy);

            fprintf(stderr, "X11 error: type=%d, serial=%lu, code=%d [%s]\n",
                event->type, event->serial, (int)event->error_code, errormsg);

            exit(0);
            return 0;
        }

        int XSSIOErrorHandlerImpl(Display* display)
        {
            exit(0);
            return 0;
        }

        void XScreensaver::Create(int argc, char* argv[])
        {
            UIManager::mScreensaverHandler = this;
            UIManager::mIsScreensaver = true;

            mWindowOverride = 0;
            mOnRoot = false;

            bool clearRoot = true;

            mLogger->TraceDebug("Arguments: ");
            for (int i = 0; i < argc; i++)
            {
                mLogger->TraceDebug(" @%02d - %s", i, argv[i]);
                if (!strcmp(argv[i], "-window-id") && i < argc - 1)
                    sscanf(argv[i + 1], "0x%lX", &mWindowOverride);
                else if (!strcmp(argv[i], "-root"))
                    mOnRoot = true;
                else if (!strcmp(argv[i], "-dontClearRoot"))
                    clearRoot = false;
            }

            XtAppContext ctx;
            XrmOptionDescRec opts;
            Widget toplevel = XtAppInitialize(&ctx, "LoLScreensaver", &opts, 0, &argc, argv, nullptr, 0, 0);

            XSetErrorHandler(XSSErrorHandlerImpl);
            XSetIOErrorHandler(XSSIOErrorHandlerImpl);

            mPixbuf = nullptr;
            mPixbufSize = 0;

            mDisplay = XtDisplay(toplevel);
            XMatchVisualInfo(mDisplay, DefaultScreen(mDisplay), 32, TrueColor, &vi);

            XWindowAttributes wa;

            mLogger->TraceDebug(
                "Creating renderer: mWindowdow=0x%x,mOnRoot=%s,clearRoot=%s\n",
                mWindowOverride,
                mOnRoot ? "true" : "false",
                clearRoot ? "true" : "false"
            );

            if (mWindowOverride)
            {
                mWindow = mWindowOverride;

                XtDestroyWidget(toplevel);
                XGetWindowAttributes(mDisplay, mWindow, &wa);

                wa.your_event_mask |= KeyPressMask | StructureNotifyMask | VisibilityChangeMask | FocusChangeMask;

                XSelectInput(mDisplay, mWindow, wa.your_event_mask);

                if (!(wa.all_event_masks & (ButtonPressMask | ButtonReleaseMask)))
                    XSelectInput(mDisplay, mWindow, wa.your_event_mask | ButtonPressMask | ButtonReleaseMask);
            }
            else if (mOnRoot)
            {
                mWindow = VirtualRootWindowOfScreen(XtScreen(toplevel));
                XtDestroyWidget(toplevel);

                XGetWindowAttributes(mDisplay, mWindow, &wa);
                XSelectInput(mDisplay, mWindow, wa.your_event_mask | StructureNotifyMask | VisibilityChangeMask | FocusChangeMask);
            }
            else
            {
                mWindow = RootWindowOfScreen(XtScreen(toplevel));
                XtDestroyWidget(toplevel);

                int screenid = DefaultScreen(mDisplay);
                mWindow = XCreateSimpleWindow(mDisplay, mWindow, 0, 0, 800, 600, 1, WhitePixel(mDisplay, screenid), BlackPixel(mDisplay, screenid));
                XMapWindow(mDisplay, mWindow);
                XMoveResizeWindow(mDisplay, mWindow, 0, 0, 800, 600);
                XGetWindowAttributes(mDisplay, mWindow, &wa);

                XSelectInput(mDisplay, mWindow, wa.your_event_mask | KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask | FocusChangeMask);
                XGetWindowAttributes(mDisplay, mWindow, &wa);
            }

            if (clearRoot) XClearWindow(mDisplay, mWindow);

            x = wa.x;
            y = wa.y;
            height = wa.height;
            width = wa.width;

            CheckFrameBuffer(width, height);
            mLogger->TraceDebug("Target window: 0x%lx [%d %d %d %d]\n", mWindow, height, width, x, y);

            gc = XCreateGC(mDisplay, mWindow, 0, nullptr);
        }

        void XScreensaver::RunEventLoop()
        {
            XEvent event;

            while (true)
            {
                while (XPending(mDisplay))
                {
                    XNextEvent(mDisplay, &event);

                    mLogger->TraceDebug("Event: %d\n", event.type);

                    if (event.type == ClientMessage || event.type == DestroyNotify)
                        goto shutdown;

                    if (mOnRoot && !mWindowOverride && event.type == UnmapNotify)
                        goto shutdown;
                }

                XWindowAttributes wa;
                XGetWindowAttributes(mDisplay, mWindow, &wa);

                if (height != wa.height || width != wa.width)
                {
                    mLogger->TraceDebug("Window resized [%dx%d] -> [%dx%d]\n", width, height, wa.width, wa.height);
                    height = wa.height;
                    width = wa.width;
                    CheckFrameBuffer(width, height);
                }

                CefDoMessageLoopWork();
                usleep(10);
            }

            shutdown:
            CefShutdown();
        }

        Window XScreensaver::GetWindow()
        {
            return mWindow;
        }

        /*static*/ void XScreensaver::CreateLaunchScript()
        {
            if (Filesystem::gExecutablePath == "/proc/self/exe")
                return;

            auto path = Filesystem::gRootDir / "LoLScreensaver";
            std::ofstream ofs(path);
            assert(ofs);

            if (stdfs::exists("/bin/sh"))
                ofs << "#!/bin/sh" << std::endl;
            else if (stdfs::exists("/bin/bash"))
                ofs << "#!/bin/bash" << std::endl;
            else if (stdfs::exists("/usr/bin/sh"))
                ofs << "#!/usr/bin/sh" << std::endl;
            else if (stdfs::exists("/usr/bin/bash"))
                ofs << "#!/usr/bin/bash" << std::endl;
            else
            {
                const char *env_shell = getenv("SHELL");
                if (env_shell && stdfs::exists(env_shell))
                    ofs << "#!" << env_shell << std::endl;
                else
                {
                    auto logger = Logging::LoggerManager::GetLogger("XScreensaver");
                    logger->TraceError("Could not figure out user's shell, this could cause screensaver malfunction");
                }
            }

            ofs << "\ncd " << Filesystem::gWorkingDirectory << std::endl;
            ofs << Filesystem::gLauncherPath << " --silent-logging --ld-mode=screensaver --ld-block-telemetry $@" << std::endl;
            ofs << "exit $?" << std::endl;

            ofs.close();
            chmod(path.c_str(), 0700);
        }

        /*static*/ void XScreensaver::ModifyXscreensaverFile()
        {
            auto logger = Logging::LoggerManager::GetLogger("XScreensaver");

            auto path = Filesystem::gUserHome / ".xscreensaver";

            if (!stdfs::exists(path))
            {
                logger->Info("xscreensaver config file does not exist, starting 'xscreensaver-demo' to create one");
                system("xscreensaver-demo &\nsleep 2 && killall xscreensaver-demo");
            }

            if (!stdfs::exists(path))
            {
                logger->TraceError("Could not create xscreensaver config file");
                exit(-1);
            }

            XScreensaverConfig cfg{path};
            cfg.AddOrModifyScreensaver("LoLScreensaver", (Filesystem::gRootDir / "LoLScreensaver").string() + " -root", "GL", true);
            cfg.Save();
        }

        // CefRenderHandler
        void XScreensaver::GetViewRect(CefRefPtr<CefBrowser> browser, CefRect &rect)
        {
            mLogger->TraceDebug("GetViewRect -> %d %d %dx%d", x, y, width, height);
            rect = CefRect(x, y, width, height);
        }

        void XScreensaver::OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList &dirtyRects, const void * buffer, int w, int h)
        {
            CheckFrameBuffer(w, h);
            memcpy(mPixbuf, buffer, w * h * 4);

            XImage* img = XCreateImage(mDisplay, vi.visual, 24, ZPixmap, 0, mPixbuf, w, h, 32, 0);

            XPutImage(mDisplay, mWindow, gc, img, 0, 0, 0, 0, width, height);
        }

        void XScreensaver::CheckFrameBuffer(int w, int h)
        {
            if (!mPixbuf || mPixbufSize < w * h * 4)
            {
                if (mPixbuf)
                {
                    mLogger->TraceDebug("Freeing old frame buffer");
                    delete[] mPixbuf;
                }

                mPixbufSize = w * h * 4;
                mPixbuf = new char[mPixbufSize];

                mLogger->TraceDebug("Allocating new framebuffer %dx%d (%d bytes)", w, h, mPixbufSize);
            }
        }
    }
}
