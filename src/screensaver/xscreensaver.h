
#ifndef LD_XSCREENSAVER_H
#define LD_XSCREENSAVER_H

#include "include/cef_render_handler.h"

#include "log.h"

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Intrinsic.h>

namespace LeagueDisplays
{
    namespace UI
    {
        int XSSErrorHandlerImpl(Display *display, XErrorEvent *event);
        int XSSIOErrorHandlerImpl(Display *display);

        class XScreensaver : public CefRenderHandler, public Logging::LoggerHolder
        {
            public:
                XScreensaver() : Logging::LoggerHolder{"XScreensaver"} { }

                void Create(int argc, char* argv[]);

                void RunEventLoop();
                Window GetWindow();

                static void CreateLaunchScript();
                static void ModifyXscreensaverFile();

                // CefRenderHandler
                virtual void GetViewRect(CefRefPtr<CefBrowser> browser, CefRect &rect) OVERRIDE;
                virtual void OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type,
                    const RectList &dirtyRects, const void* buffer, int w, int h) OVERRIDE;
            private:
                int width,
                    height,
                    x,
                    y;

                char                   *mPixbuf;
                size_t                  mPixbufSize;

                Window                  root, mWindow, mWindowOverride;
                Display                *mDisplay;
                XVisualInfo             vi;
                Colormap                cmap;
                XSetWindowAttributes    swa;
                GC                      gc;
                bool                    mOnRoot;

                void CheckFrameBuffer(int w, int h);

                IMPLEMENT_REFCOUNTING(XScreensaver);
        };
    }
}

#endif // LD_XSCREENSAVER_H
