
#include "xscreensaver_config.h"

#include <fstream>
#include <sstream>
#include <cstdio>
#include <assert.h>

namespace LeagueDisplays
{
    XScreensaverConfig::XScreensaverConfig(stdfs::path file)
    {
        mFilePath = file;
        Reload();
    }

    void XScreensaverConfig::Reload()
    {
        std::ifstream infile{mFilePath};
        std::string line, key, value;
        size_t pos;
        bool split_line = false;
        int lineno = 0;

        mConfig.clear();
        while (std::getline(infile, line))
        {
            lineno++;
            pos = line.find('#');

            if (pos != std::string::npos)
                line = line.substr(0, pos);

            while (line[0] == ' ' || line[0] == '\t')
                line = line.substr(1);

            if (line.empty())
            {
                split_line = false;
                continue;
            }

            if (!split_line)
            {
                // Store key/value pair
                if (!key.empty() && !value.empty())
                    mConfig.insert({key, value});

                // Get config key & value
                pos = line.find(':');

                if (pos != std::string::npos)
                {
                    key = line.substr(0, pos);
                    value = line.substr(pos + 1);

                    while (value[0] == ' ' || value[0] == '\t')
                        value = value.substr(1);
                }
                else
                {
                    printf("Syntax error: missing ':' in line %d\n", lineno);
                    continue;
                }

                while (value.length() && (value[value.length() - 1] == ' ' || value[value.length() - 1] == '\t'))
                    value = value.substr(0, value.length() - 1);

                if (value[value.length() - 1] == '\\')
                {
                    split_line = true;
                    value = value.substr(0, value.length() - 1);
                }

                while ((pos = value.find("\\n")) != std::string::npos)
                    value = value.substr(0, pos) + "\n" + value.substr(pos + 2);

                continue;
            }
            else
            {
                split_line = false;

                while (line.length() && (line[line.length() - 1] == ' ' || line[line.length() - 1] == '\t'))
                    line = line.substr(0, line.length() - 1);

                if (line[line.length() - 1] == '\\')
                {
                    split_line = true;
                    line = line.substr(0, line.length() - 1);
                }

                while ((pos = line.find("\\n")) != std::string::npos)
                    line = line.substr(0, pos) + "\n" + line.substr(pos + 2);

                value += line;
                continue;
            }
        }

        // Store key/value pair
        if (!key.empty() && !value.empty())
            mConfig.insert({key, value});

        mPrograms.clear();
        if (HasKey("programs"))
        {
            std::stringstream ss(GetKeyValue("programs"));

            XScreensaverProgram prg;

            while (std::getline(ss, line))
            {
                prg.enabled = line[0] != '-';

                while (line[0] == '-' || line[0] == ' ' || line[0] == '\t')
                    line = line.substr(1);

                while (line[line.length() - 1] == ' ' || line[line.length() - 1] == '\t')
                    line = line.substr(0, line.length() - 1);

                if ((pos = line.find(':')) != std::string::npos && pos < 40 /*?*/)
                {
                    prg.mode = line.substr(0, pos);
                    line = line.substr(pos + 1);

                    while (line[0] == ' ' || line[0] == '\t')
                        line = line.substr(1);
                }
                else
                {
                    prg.mode = "";
                }

                prg.commandline = line;

                auto pos = prg.commandline.find(' ');
                if (pos != std::string::npos)
                    prg.exe = prg.commandline.substr(0, pos);
                else
                    prg.exe = prg.commandline;

                prg.name = prg.exe.filename();

                mPrograms.push_back(prg);
            }
        }
    }

    void XScreensaverConfig::Save()
    {
        SaveAs(mFilePath);
    }

    void XScreensaverConfig::SaveAs(stdfs::path file)
    {
        std::ofstream ofs{file};
        assert(ofs);

        ofs << "# XScreenSaver Preferences File" << std::endl;
        ofs << "# Written by leaguedisplays" << std::endl;
        ofs << std::endl;

        if (mPrograms.size() > 0)
        {
            std::string val = "";

            for (auto prg : mPrograms)
            {
                if (!prg.enabled)
                    val += "- ";

                if (prg.mode != "")
                    val += prg.mode + ": ";

                val += "                              ";
                val += prg.commandline + "           \n";
            }

            SetKeyValue("programs", val);
        }

        size_t pos, pos2;
        std::string value;

        for (auto p : mConfig)
        {
            value = p.second;

            pos2 = 0;

            // Don't ask, this works
            while ((pos = value.substr(pos2).find("\n")) != std::string::npos)
            {
                value = value.substr(0, pos2 + pos) + "\\n\\" + value.substr(pos2 + pos);
                pos2 = pos2 + pos + 4;
            }

            ofs << p.first << ": " << value << std::endl;
        }

        ofs.close();
    }

    bool XScreensaverConfig::HasKey(std::string name)
    {
        return mConfig.find(name) != mConfig.end();
    }

    std::string XScreensaverConfig::GetKeyValue(std::string name)
    {
        if (!HasKey(name))
            throw std::invalid_argument("Key does not exist");

        return mConfig.at(name);
    }

    void XScreensaverConfig::SetKeyValue(std::string name, std::string value)
    {
        if (mConfig.find(name) != mConfig.end())
            mConfig.erase(mConfig.find(name));

        mConfig.insert(std::pair<std::string, std::string>(name, value));
    }

    bool XScreensaverConfig::ScreensaverExists(std::string name)
    {
        for (auto prg : mPrograms)
            if (prg.name == name)
                return true;

        return false;
    }

    std::string XScreensaverConfig::GetScreensaverCommandLine(std::string name)
    {
        for (auto prg : mPrograms)
            if (prg.name == name)
                return prg.commandline;

        return "";
    }

    void XScreensaverConfig::AddOrModifyScreensaver(std::string name, std::string commandline, std::string mode, bool enabled)
    {
        for (auto& prg : mPrograms)
            if (prg.name == name)
            {
                prg.enabled = enabled;
                prg.mode = std::move(mode);
                prg.commandline = std::move(commandline);
                return;
            }

        XScreensaverProgram newprg;
        newprg.name = std::move(name);
        newprg.commandline = std::move(commandline);
        newprg.enabled = enabled;
        newprg.mode = std::move(mode);

        mPrograms.push_back(newprg);
    }
}
