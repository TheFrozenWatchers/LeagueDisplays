
#include "filesystem.h"
#include "log.h"

#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstring>
#include <fstream>
#include <iterator>
#include <limits.h>

namespace LeagueDisplays
{
    /* static */ stdfs::path Filesystem::gWorkingDirectory;
    /* static */ stdfs::path Filesystem::gExecutablePath;
    /* static */ stdfs::path Filesystem::gLauncherPath;
    /* static */ stdfs::path Filesystem::gUserHome;
    /* static */ stdfs::path Filesystem::gRootDir;
    /* static */ stdfs::path Filesystem::gCefCacheDir;
    /* static */ stdfs::path Filesystem::gDefaultDownloadDir;
    /* static */ stdfs::path Filesystem::gFilesystemCacheDir;
    /* static */ stdfs::path Filesystem::gDebugLogPath;
    /* static */ stdfs::path Filesystem::gSettingsFilePath;
    /* static */ stdfs::path Filesystem::gPlaylistsFilePath;

    /* static */ std::shared_ptr<Logging::Logger> Filesystem::gLogger;

    /* static */ void Filesystem::Initialize()
    {
        gLogger = Logging::LoggerManager::GetLogger("Filesystem");

        gLogger->Info("Initializing filesystem API");

        std::unique_ptr<char[]> path_tmp{new char[PATH_MAX]};
        
        realpath("/proc/self/exe", path_tmp.get());
        Filesystem::gExecutablePath = path_tmp.get();

        gLogger->Info("Executable is %s", Filesystem::gExecutablePath.c_str());

        gWorkingDirectory = Filesystem::gExecutablePath.parent_path();

        gLogger->Info("Working directory is %s", gWorkingDirectory.c_str());

        const char* userhome = getenv("HOME");
        if (!stdfs::exists(userhome))
        {
            gLogger->TraceError("HOME is not defined using working directory instead!");
            userhome = "./";
        }

        if (strlen(userhome) <= 0)
        {
            gLogger->TraceError("Could not get root dir path - FATAL");
            abort();
        }

        gRootDir = userhome;
        gUserHome = gRootDir;

        gLauncherPath = gWorkingDirectory / "leaguedisplays";

        gDefaultDownloadDir = gRootDir / "Downloads/LeagueDisplays/";
        stdfs::create_directories(gDefaultDownloadDir);

        gLogger->Info("Default download directory is %s", gDefaultDownloadDir.c_str());

        gRootDir /= ".leaguedisplays/";
        stdfs::create_directories(gRootDir);
        gLogger->Info("Root directory is %s", gRootDir.c_str());

        gDebugLogPath = gRootDir / "debug.log";
        gCefCacheDir = gRootDir / "cefCache/";
        stdfs::create_directories(gCefCacheDir);

        gLogger->Info("CEF cache directory is %s", gCefCacheDir.c_str());

        gFilesystemCacheDir = gRootDir / "filesystem/";
        gLogger->Info("Filesystem cache directory is %s", gFilesystemCacheDir.c_str());

        gSettingsFilePath = gFilesystemCacheDir / "LeagueScreensaver/settings.json";
        gPlaylistsFilePath = gFilesystemCacheDir / "LeagueScreensaver/playlists.json";

        if (!stdfs::exists(gSettingsFilePath))
            WriteAllText(gSettingsFilePath, "{}");
        else
            gLogger->Info("Config file [settings] already exists at %s", gSettingsFilePath.c_str());

        if (!stdfs::exists(gPlaylistsFilePath))
            WriteAllText(gPlaylistsFilePath, "{}");
        else
            gLogger->Info("Config file [playlists] already exists at %s", gPlaylistsFilePath.c_str());
    }

    /* static */ void Filesystem::ScreensaverInit()
    {
        gDebugLogPath = gRootDir / "screensaver-debug.log";
        gCefCacheDir = gRootDir / "SSCefCache/";
        stdfs::create_directories(gCefCacheDir);

        gLogger->Info("Changed debug log path to %s", gDebugLogPath.c_str());
        gLogger->Info("Changed cache dir to %s", gCefCacheDir.c_str());
    }

    /* static */ void Filesystem::FileMkdirs(stdfs::path path)
    {
        if (stdfs::is_directory(path))
        {
            stdfs::create_directories(path);
            return;
        }

        stdfs::create_directories(path.parent_path());
    }

    /* static */ void Filesystem::WriteAllText(stdfs::path path, std::string text)
    {
        std::ofstream ofs(path);

        if (!ofs || !ofs.is_open())
        {
            gLogger->TraceError("Failed to open file (%s) for writing", path.c_str());
            return;
        }

        ofs << text;
    }

    /* static */ std::vector<unsigned char> Filesystem::ReadAllBytes(stdfs::path path)
    {
        std::ifstream ifs{path, std::ios::in | std::ios::binary};
        ifs.unsetf(std::ios::skipws);

        ifs.seekg(0, std::ios::end);
        auto fileSize = ifs.tellg();
        ifs.seekg(0, std::ios::beg);

        std::vector<unsigned char> buffer;
        buffer.reserve(fileSize);

        buffer.insert(
            buffer.begin(),
            std::istream_iterator<unsigned char>{ifs},
            std::istream_iterator<unsigned char>{}
        );

        return std::move(buffer);
    }

    /* static */ int Filesystem::_JZProcessFile(JZFile* zip, std::string targetDir)
    {
        JZFileHeader header;
        char filename[1024];

        if (jzReadLocalFileHeader(zip, &header, filename, sizeof(filename)))
        {
            gLogger->TraceError("Couldn't read local file header!");
            return -1;
        }

        std::string path = targetDir + filename;

        std::unique_ptr<unsigned char[]> data{new unsigned char[header.uncompressedSize]};

        if (jzReadData(zip, &header, data.get()))
        {
            gLogger->TraceError("Couldn't read file data!");
            return -1;
        }

        if (path[path.length() - 1] != '/' && !stdfs::exists(path))
        {
            gLogger->TraceDebug("Unpacking %s [size: %d bytes]", filename, header.uncompressedSize);

            FileMkdirs(path);

            std::ofstream ofs{path, std::ios::out | std::ios::binary};

            if (!ofs || !ofs.is_open())
            {
                gLogger->TraceError("Could not write file: %s", path.c_str());
                return -1;
            }

            ofs.write((char*)data.get(), header.uncompressedSize);
        }

        return 0;
    }

    /* static */ int Filesystem::_JZRecordCallback(JZFile* zip, int idx, JZFileHeader* header, char* filename, void* user_data)
    {
        const std::string* targetDir = (const std::string*)user_data;

        long offset = zip->tell(zip); // store current position

        if (zip->seek(zip, header->offset, SEEK_SET))
        {
            gLogger->TraceError("Cannot seek in zip file!");
            return 0; // abort
        }

        _JZProcessFile(zip, *targetDir); // alters file offset

        zip->seek(zip, offset, SEEK_SET); // return to position

        return 1; // continue
    }

    /*static*/ bool Filesystem::Unzip(stdfs::path file, std::string targetDir)
    {
        gLogger->TraceDebug("Unzip: %s -> %s", file.c_str(), targetDir.c_str());

        stdfs::create_directories(targetDir);
        bool ret = false;

        if (!stdfs::exists(file))
            return false;

        FILE *f = fopen(file.c_str(), "rb");
        if (!f)
        {
            gLogger->TraceError("Failed to open file (%s) for reading", file.c_str());
            return false;
        }

        JZFile *zip = jzfile_from_stdio_file(f);
        if (!zip)
        {
            gLogger->TraceError("Failed create JZFile from FILE*");
            fclose(f);
            return false;
        }

        JZEndRecord endRecord;

        if (jzReadEndRecord(zip, &endRecord))
        {
            gLogger->TraceError("Couldn't read ZIP file end record.");
            goto failed;
        }

        if (jzReadCentralDirectory(zip, &endRecord, &Filesystem::_JZRecordCallback, (void*)&targetDir))
        {
            gLogger->TraceError("Couldn't read ZIP file central record.");
            goto failed;
        }

        ret = true;

        failed:
        zip->close(zip);
        fclose(f);
        return ret;
    }

    /*static*/ void Filesystem::PIDFileSet(std::string name, unsigned short pid)
    {
        WriteAllText(gRootDir / name, std::to_string(pid));
    }

    /*static*/ unsigned short Filesystem::PIDFileGet(std::string name)
    {
        std::ifstream ifs{gRootDir / name};

        if (!ifs)
            return 0;

        int p = 0;
        ifs >> p;

        return p & 0xFFFF;
    }
}
