
#ifndef LD_FILESYSTEM_API_H
#define LD_FILESYSTEM_API_H

#include <string>
#include <vector>

#if __cplusplus >= 201703L
#include <filesystem>
#else
#include <experimental/filesystem>
#endif

#include "junzip.h"
#include "log.h"

namespace LeagueDisplays
{
    #if __cplusplus >= 201703L
    namespace stdfs = std::filesystem;
    #else
    namespace stdfs = std::experimental::filesystem;
    #endif

    struct Filesystem
    {
        static void Initialize();
        static void ScreensaverInit();

        static void WriteAllText(stdfs::path path, std::string text);
        static std::vector<unsigned char> ReadAllBytes(stdfs::path path);

        static void FileMkdirs(stdfs::path path);

        static bool Unzip(stdfs::path file, std::string targetDir);

        static void PIDFileSet(std::string, unsigned short pid);
        static unsigned short PIDFileGet(std::string);

        static stdfs::path gWorkingDirectory;
        static stdfs::path gExecutablePath;
        static stdfs::path gLauncherPath;
        static stdfs::path gUserHome;
        static stdfs::path gRootDir;
        static stdfs::path gCefCacheDir;
        static stdfs::path gDefaultDownloadDir;
        static stdfs::path gFilesystemCacheDir;
        static stdfs::path gDebugLogPath;
        static stdfs::path gSettingsFilePath;
        static stdfs::path gPlaylistsFilePath;

        private:
            static int _JZProcessFile(JZFile* zip, std::string targetDir);
            static int _JZRecordCallback(JZFile* zip, int idx, JZFileHeader* header, char* filename, void* user_data);

            static std::shared_ptr<Logging::Logger> gLogger;
    };
}

#endif
