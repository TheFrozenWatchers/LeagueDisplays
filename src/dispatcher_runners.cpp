
#include <vector>

#include "dispatcher.h"
#include "filesystem.h"
#include "crossde.h"
#include "misc.hpp"
#include "background_daemon.h"

#include "ui/uibase.h"

#include "screensaver/xscreensaver.h"

#include "cef/client_app.h"
#include "cef/client_handler.h"
#include "cef/scheme_handlers.h"
#include "cef/screensaver_app.h"
#include "cef/telemetry_filter.hpp"

#include <unistd.h>
#include <signal.h>

namespace LeagueDisplays
{
    struct StartupCEFApp : StartupRunner<CONSTSTR("cef-app")>
    {
        int Run(const StartupContext& ctx) override
        {
            CefRefPtr<CEF::ClientApp> app{new CEF::ClientApp};
            int exit_code = CefExecuteProcess(ctx.mMainArgs, app.get(), NULL);

            if (exit_code >= 0)
                return exit_code;

            Logging::LoggerManager::Instance().EnableAsyncLogging();

            // Specify CEF global settings here.
            CefSettings settings;
            settings.no_sandbox = true;
            settings.log_severity = LOGSEVERITY_DISABLE; // LOGSEVERITY_VERBOSE;
            settings.remote_debugging_port = 9222;

            CefString(&settings.user_agent).FromASCII("LeagueDisplays");
            CefString(&settings.log_file).FromString(Filesystem::gDebugLogPath);
            CefString(&settings.cache_path).FromString(Filesystem::gCefCacheDir);

            CefInitialize(ctx.mMainArgs, settings, app.get(), NULL);

            mLogger->Info("Started message loop");
            CefRunMessageLoop();
            mLogger->Info("Finished message loop");
            return 0;
        }
    };

    struct StartupScreensaver : StartupRunner<CONSTSTR("screensaver")>
    {
        int Run(const StartupContext& ctx) override
        {
            char **argv2 = (char**)malloc(sizeof(char*) * ctx.mMainArgs.argc);
            memcpy(argv2, ctx.mMainArgs.argv, sizeof(char*) * ctx.mMainArgs.argc);

            if (!ctx.mCmdline->HasSwitch("ld-scr-root-process"))
                ctx.mCmdline->AppendSwitchWithValue("ld-scr-root-process", std::to_string(getpid()));

            Filesystem::ScreensaverInit();

            CefRefPtr<CEF::ScreensaverApp> app(new CEF::ScreensaverApp);
            int exit_code = CefExecuteProcess(ctx.mMainArgs, app.get(), NULL);

            if (exit_code >= 0)
                return exit_code;

            Logging::LoggerManager::Instance().EnableAsyncLogging();

            // Specify CEF global settings here.
            CefSettings settings;
            settings.no_sandbox = 1;
            settings.windowless_rendering_enabled = 1;
            settings.background_color = 0xFF000000;
            settings.log_severity = LOGSEVERITY_DISABLE;

            CefString(&settings.user_agent).FromASCII("LeagueDisplays");
            CefString(&settings.log_file).FromString(Filesystem::gDebugLogPath);
            CefString(&settings.cache_path).FromString(Filesystem::gCefCacheDir);

            if (!CefInitialize(ctx.mMainArgs, settings, app.get(), NULL))
                return -1;

            if (!stdfs::exists(Filesystem::gFilesystemCacheDir / "screensaver/"))
            {
                if (!Filesystem::Unzip("screensaver-webapp.bin", Filesystem::gFilesystemCacheDir / "screensaver/"))
                {
                    mLogger->TraceError("Screensaver assets are missing!");
                    return -1;
                }
            }

            auto xscr = std::make_unique<UI::XScreensaver>();

            int pid = atoi(ctx.mCmdline->GetSwitchValue("ld-scr-root-process").ToString().c_str());

            if (pid == 0 || kill(pid, 0))
            {
                mLogger->TraceError("Invalid root process PID: %d - FATAL", pid);
                return 1;
            }

            // Restore command line from the root process
            // This way we have the original Xorg/Xscreensaver arguments
            auto data = Filesystem::ReadAllBytes("/proc/" + std::to_string(pid) + "/cmdline");
            std::vector<char*> argv;

            char* beg = (char*)data.data();
            for (size_t i = 1; i < data.size(); i++)
            {
                if (data[i] == '\0' || data[i] == ' ')
                {
                    data[i] = 0;
                    argv.push_back(beg);
                    beg = (char*)&data[i+1];
                }
            }

            xscr->Create(argv.size(), argv.data());

            std::string url;

            url = CefCommandLine::GetGlobalCommandLine()->GetSwitchValue("url-override");
            if (url.empty())
                url = "http://filesystem/screensaver/index.html";

            // ClientHandler implements browser-level callbacks.
            CefRefPtr<CEF::ClientHandler> handler(new CEF::ClientHandler());

            // Specify CEF browser settings here.
            CefBrowserSettings browser_settings;
            browser_settings.web_security = STATE_DISABLED;
            browser_settings.windowless_frame_rate = 60;
            CefString(&browser_settings.default_encoding).FromASCII("UTF-8");

            CefRegisterSchemeHandlerFactory("http", "filesystem",
                new CEF::BasicSchemeHandlerFactory<CEF::FilesystemResourceHandler>());

            CefRefPtr<CefRequestContext> reqCtx = nullptr;
            
            if (ctx.mCmdline->HasSwitch("ld-block-telemetry"))
            {
                reqCtx = CefRequestContext::CreateContext(
                    CefRequestContext::GetGlobalContext(),
                    new CEF::TelemetryFilterContextHandler
                );
            }

            CefWindowInfo window_info;
            window_info.SetAsWindowless(xscr->GetWindow());

            CefBrowserHost::CreateBrowserSync(window_info, handler, url, browser_settings, nullptr, reqCtx);

            mLogger->Info("Started message loop");
            xscr->RunEventLoop();
            mLogger->Info("Finished message loop");

            return 0;
        }
    };

    struct StartupAgent : StartupRunner<CONSTSTR("agent")>
    {
        int Run(const StartupContext& ctx) override
        {
            Logging::LoggerManager::Instance().EnableAsyncLogging();

            unsigned short pid = Filesystem::PIDFileGet("agent.pid");

            if (pid && pid != getpid() && !kill(pid, 0))
            {
                mLogger->TraceError("Agent process is already running!");
                return 0;
            }

            Filesystem::PIDFileSet("agent.pid", getpid());

            DesktopEnvApi::LogDesktopEnv();
            BackgroundDaemon::Start();

            CefMainArgs args{ctx.mMainArgs};
            gtk_init(&args.argc, &args.argv);

            UI::UIManager::CreateAgent();
            gtk_main();

            Filesystem::PIDFileSet("agent.pid", 0);
            return 0;
        }
    };

    // This is the default startup runner
    struct StartupLaunch : StartupRunner<CONSTSTR("launch")>
    {
        int Run(const StartupContext& ctx) override
        {
            unsigned short pid = Filesystem::PIDFileGet("agent.pid");

            if (!pid || kill(pid, 0))
            {
                mLogger->Info("Agent is not running, starting it!");

                std::string cmd = "\"" + Filesystem::gExecutablePath.string() + "\" --tray --ld-fork";

                mLogger->Info("Launching agent with command: '%s'", cmd.c_str());

                int r = system(cmd.c_str());

                if (r < 0)
                {
                    mLogger->TraceError("Could not start agent!");
                    return r;
                }

                sleep(2);
                pid = Filesystem::PIDFileGet("agent.pid");
            }

            std::stringstream cmd;
            cmd << "\"" << Filesystem::gExecutablePath << "\"";
            cmd << " --ld-mode=cef-app";
            cmd << " --ld-fork";

            if (ctx.mCmdline->HasSwitch("ld-block-telemetry"))
                cmd << " --ld-block-telemetry";

            int r = system(cmd.str().c_str());

            if (r < 0)
            {
                mLogger->TraceError("Could not start app!");
                return r;
            }

            return 0;
        }
    };

    void StartupDispatcher::RegisterRunners()
    {
        auto& disptch = StartupDispatcher::Instance();

        disptch.Register<StartupCEFApp>();
        disptch.Register<StartupScreensaver>();
        disptch.Register<StartupAgent>();
        disptch.Register<StartupLaunch>();
    }
}
