
#ifndef MISC_H
#define MISC_H

#include <string>
#include <sstream>
#include <utility>
#include <array>
#include <vector>

#define CONSTSTR(s) decltype(s##_cstr)

namespace LeagueDisplays
{
    namespace Misc
    {
        static const unsigned int FNV_PRIME = 16777619u;
        static const unsigned int OFFSET_BASIS = 2166136261u;

        template <unsigned int N>
        static inline constexpr unsigned int fnvHashConst(const char (&str)[N], unsigned int I = N) noexcept {
            return I == 1 ? (OFFSET_BASIS ^ str[0]) * FNV_PRIME : (fnvHashConst(str, I - 1) ^ str[I - 1]) * FNV_PRIME;
        }

        template <unsigned long N>
        static constexpr unsigned int fnvHashConst(const std::array<char, N> str, unsigned long I = N) {
            return I == 1 ? (OFFSET_BASIS ^ str[0]) * FNV_PRIME : (fnvHashConst(str, I - 1) ^ str[I - 1]) * FNV_PRIME;
        }

        static inline unsigned int fnvHash(std::string str)
        {
            unsigned int hash = OFFSET_BASIS;

            for (char c : str)
            {
                hash ^= c;
                hash *= FNV_PRIME;
            }

            hash ^= 0;
            hash *= FNV_PRIME;

            return hash;
        }

        template<char... str>
        struct ConstString
        {
            static inline constexpr char value[sizeof...(str)+1] = {str..., '\0'};
            static inline constexpr unsigned int length = sizeof...(str);
            static inline constexpr unsigned int hash = fnvHashConst(value);
        };

        template<unsigned N1, typename T1, T1... Nums1, unsigned N2, typename T2, T2... Nums2>
        static inline constexpr const std::array<char, N1+N2> const_concat_helper(const char (&str1)[N1], std::integer_sequence<T1, Nums1...>, const char (&str2)[N2], std::integer_sequence<T2, Nums2...>) noexcept {
            return {str1[Nums1]...,str2[Nums2]...};
        }

        template<unsigned N1, unsigned N2>
        static inline constexpr std::array<char, N1+N2> const_concat(const char (&str1)[N1], const char (&str2)[N2]) noexcept {
            return const_concat_helper(str1, std::make_integer_sequence<unsigned, N1-1>(), str2, std::make_integer_sequence<unsigned, N2>());
        }

        template<typename S1, typename S2>
        struct ConcatConstStrings
        {
            static inline constexpr const char value[S1::length+S2::length+1] = const_concat(S1::value, S2::value);
        };

        static inline constexpr char cosnt_replace_char_helper(const char c, const char search, const char replacement) noexcept {
            return c == search ? replacement : c;
        }

        template <unsigned N, typename T, T... Nums>
        static inline constexpr const std::array<char, N> const_replace_helper(const char (&str)[N], std::integer_sequence<T, Nums...>, const char search, const char replacement) noexcept {
            return {cosnt_replace_char_helper(str[Nums], search, replacement)...};
        }

        template <unsigned N>
        static inline constexpr const std::array<char, N> const_replace(const char (&str)[N], const char search, const char replacement) noexcept {
            return const_replace_helper(str, std::make_integer_sequence<unsigned, N>(), search, replacement);
        }

        template<typename Out>
        static inline void split(std::string s, char delim, Out result) {
            std::stringstream ss{std::move(s)};
            std::string item;

            while (std::getline(ss, item, delim))
                *(result++) = item;
        }

        static inline std::vector<std::string> split(std::string s, char delim) {
            std::vector<std::string> elems;
            split(std::move(s), delim, std::back_inserter(elems));
            return elems;
        }
    }

    template<typename T, T... chars>
    static inline constexpr unsigned int operator""_fnv() {
        return Misc::fnvHashConst({ chars..., '\0' });
    }

    template<typename T, T... chars>
    static inline constexpr Misc::ConstString<chars...> operator""_cstr() { return {}; }
}

#endif
