
#ifndef LD_UIBASE_H
#define LD_UIBASE_H

#pragma once

#include <X11/Xlib.h>
#include <gtk/gtk.h>
#include <string>
#include <vector>
#include <functional>

#include "log.h"
#include "screensaver/xscreensaver.h"

#ifdef LD_USE_APPINDICATOR
#include <libappindicator/app-indicator.h>
#endif

#define LD_ICON_PATH "icon.png"
#define LD_APPNAME   "League Displays"

namespace LeagueDisplays
{
    namespace UI
    {
        void _on_sig_destroy(int sig);
        int XIOErrorHandlerImpl(Display *display);
        int XErrorHandlerImpl(Display *display, XErrorEvent *event);

        struct UIManager
        {
            static void RegisterSignalHandlers();
            static void InitForCEF();
            static void CreateAgent();
            static void CloseGracefully();
            static bool SaveFileDialog(std::string filename, std::string current_name, std::string& res);
            static void ShowDownloadFinishedDialog();

            static int                          mWindowWidth;
            static int                          mWindowHeight;

#ifdef LD_USE_APPINDICATOR
            static AppIndicator*                mStatusIconAI;
#endif

            static GtkStatusIcon*               mStatusIcon;

            static bool                         mDisableAppIndicator;

            static GtkMenu*                     mStatusIconMenu;
            static bool                         mIsAllowedToExit;
            static bool                         mIsScreensaver;
            static bool                         mAllowMultipleInstances;
            static unsigned short               mHostPID;
            static CefRefPtr<XScreensaver>      mScreensaverHandler;

            static Logging::LoggerPtr gLogger;
        };
    }
}

#endif
