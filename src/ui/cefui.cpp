
#include "cefui.h"

namespace LeagueDisplays
{
    namespace UI
    {
        namespace CEF
        {
            void WindowDelegate::OnWindowCreated(CefRefPtr<CefWindow> window)
            {
                // Load icon
                auto iconImg = CefImage::CreateImage();

                auto icon_data = Filesystem::ReadAllBytes(LD_ICON_PATH);
                iconImg->AddPNG(1.0f, icon_data.data(), icon_data.size());

                // Add the browser view and show the window.
                window->AddChildView(mBrowserView);
                window->SetTitle("League Displays");
                window->SetWindowIcon(iconImg);
                window->SetWindowAppIcon(iconImg);
                window->Show();
                window->CenterWindow({UIManager::mWindowWidth, UIManager::mWindowHeight});

                // Give keyboard focus to the browser view.
                mBrowserView->RequestFocus();
            }

            void WindowDelegate::OnWindowDestroyed(CefRefPtr<CefWindow> window)
            {
                mBrowserView = nullptr;
            }

            bool WindowDelegate::CanClose(CefRefPtr<CefWindow> window)
            {
                // Allow the window to close if the browser says it's OK.
                CefRefPtr<CefBrowser> browser = mBrowserView->GetBrowser();

                if (browser)
                    return browser->GetHost()->TryCloseBrowser();

                return true;
            }

            CefSize WindowDelegate::GetPreferredSize(CefRefPtr<CefView> view)
            {
                return CefSize(UIManager::mWindowWidth, UIManager::mWindowHeight);
            }


            bool BrowserViewDelegate::OnPopupBrowserViewCreated(CefRefPtr<CefBrowserView> browser_view,
                CefRefPtr<CefBrowserView> popup_browser_view,
                bool is_devtools)
            {
                CefWindow::CreateTopLevelWindow(new WindowDelegate(popup_browser_view));
                return true;
            }
        }
    }
}
