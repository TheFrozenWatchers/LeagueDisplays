
#include <string>

#include "uibase.h"
#include "log.h"
#include "background_daemon.h"
#include "filesystem.h"
#include "crossde.h"

#include "include/cef_app.h"
#include "include/wrapper/cef_helpers.h"

#include <nfd.h>

#include <math.h>
#include <signal.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>

#include <gdk/gdkx.h>
#include <gdk/gdkkeysyms-compat.h>
#include <gdk/x11/gdkx11window.h>

namespace LeagueDisplays
{
    namespace UI
    {
        // X11 window hints
        struct Hints {
            unsigned long   flags;
            unsigned long   functions;
            unsigned long   decorations;
            long            inputMode;
            unsigned long   status;
        };

        /* static */ int UIManager::mWindowWidth = 1280;
        /* static */ int UIManager::mWindowHeight = 720;

#ifdef LD_USE_APPINDICATOR
        /* static */ AppIndicator* UIManager::mStatusIconAI = NULL;
#endif
        /* static */ GtkStatusIcon* UIManager::mStatusIcon = NULL;

        /* static */ bool UIManager::mDisableAppIndicator = false;

        /* static */ GtkMenu* UIManager::mStatusIconMenu = NULL;

        /* static */ CefRefPtr<XScreensaver> UIManager::mScreensaverHandler = NULL;

        /* static */ bool UIManager::mIsAllowedToExit = false;
        /* static */ bool UIManager::mIsScreensaver = false;
        /* static */ bool UIManager::mAllowMultipleInstances = false;

        /* static */ unsigned short UIManager::mHostPID = 0xFFFF;

        /* static */ Logging::LoggerPtr UIManager::gLogger;

        int XErrorHandlerImpl(Display* display, XErrorEvent* event)
        {
            printf("X11 error: type=%d, serial=%lu, code=%d",
                event->type, event->serial, (int)event->error_code);
            return 0;
        }

        int XIOErrorHandlerImpl(Display* display)
        {
            puts("XIOErrorHandlerImpl");
            return 0;
        }

        void agent_action(gchar* data)
        {
            if (!strcmp(data, "sigquit"))
            {
                unsigned short pid = Filesystem::PIDFileGet("cefapp.pid");

                if (pid && !kill(pid, 0))
                {
                    kill(pid, SIGTERM);
                    sleep(2);
                    kill(pid, SIGKILL);
                }

                exit(0);
            }
            else if (!strcmp(data, "sigopen"))
            {
                unsigned short pid = Filesystem::PIDFileGet("cefapp.pid");

                if (!pid || kill(pid, 0))
                {
                    std::string cmd = "\"" + Filesystem::gExecutablePath.string() + "\" --ld-mode=cef-app --ld-fork";

                    int r = system(cmd.c_str());

                    if (r < 0) UIManager::gLogger->TraceError("Could not start CEF process!");
                }
            }
            else if (!strcmp(data, "signext"))
                BackgroundDaemon::Next();
        }

        void _on_sig_destroy(int sig)
        {
            if (getpid() != UIManager::mHostPID)
            {
                UIManager::gLogger->TraceDebug("We are not the host, transfering signal to %d", UIManager::mHostPID);
                kill(UIManager::mHostPID, SIGINT);
                sleep(2);
                kill(UIManager::mHostPID, SIGKILL);
                return;
            }

            // To overwrite the ugly ^C in terminals
            usleep(200 * 1000);
            printf("\r");
            fflush(stdout);

            UIManager::gLogger->TraceDebug("Terminating via signal: %d", sig);
            UIManager::CloseGracefully();
        }

        gint tray_icon_on_click(GtkStatusIcon *status_icon, GdkEventButton *event, gpointer user_data)
        {
            if (event->type == GDK_2BUTTON_PRESS)
            {
                agent_action("sigopen");
                return TRUE;
            }

            return FALSE;
        }

        gint tray_icon_on_menu(GtkStatusIcon *status_icon, guint button, guint activate_time, gpointer user_data)
        {
            gtk_menu_popup(UIManager::mStatusIconMenu, NULL, NULL, NULL, user_data, button, activate_time);

            return FALSE;
        }

        void sig_open_cef(int sig)
        {
            agent_action("sigopen");
        }

        /* static */ void UIManager::RegisterSignalHandlers()
        {
            signal(SIGINT, _on_sig_destroy);
            signal(SIGTERM, _on_sig_destroy);
            signal(SIGUSR1, sig_open_cef);
        }

        /* static */ void UIManager::InitForCEF()
        {
            gLogger = Logging::LoggerManager::GetLogger("UIBase");

            Display* display = XOpenDisplay(NULL);
            Screen* screen = DefaultScreenOfDisplay(display);

            gLogger->Info("Max resolution is $$6%d$$rx$$6%d", screen->width, screen->height);

            double height = screen->height / 1.5;
            double width = height * (16.0 / 9.0);

            if (width > screen->width)
                width = height * (4.0 / 3.0);

            mWindowWidth  = floor(width);
            mWindowHeight = floor(height);

            gLogger->Info("Window target size is $$6%d$$rx$$6%d", mWindowWidth, mWindowHeight);

            // Add X11 error handlers
            XSetErrorHandler(XErrorHandlerImpl);
            XSetIOErrorHandler(XIOErrorHandlerImpl);

            XCloseDisplay(display);
        }

        /* static */ void UIManager::CreateAgent()
        {
            gLogger = Logging::LoggerManager::GetLogger("UIBase");

            // Create context menu
            GtkWidget* widget = gtk_menu_new();
            GtkWidget* menu_items;

            // The "widget" is the menu that was supplied when
            // g_signal_connect_swapped() was called.
            mStatusIconMenu = GTK_MENU(widget);

            menu_items = gtk_menu_item_new_with_label("League Displays");
            gtk_widget_set_sensitive(menu_items, FALSE);
            gtk_menu_shell_append (GTK_MENU_SHELL(mStatusIconMenu), menu_items);
            gtk_widget_show(menu_items);

            menu_items = gtk_separator_menu_item_new();
            gtk_menu_shell_append (GTK_MENU_SHELL(mStatusIconMenu), menu_items);
            gtk_widget_show(menu_items);

            menu_items = gtk_menu_item_new_with_label("Open configurator");
            gtk_menu_shell_append(GTK_MENU_SHELL(mStatusIconMenu), menu_items);
            g_signal_connect_swapped(menu_items, "activate", G_CALLBACK(agent_action), (gpointer) g_strdup("sigopen"));
            gtk_widget_show(menu_items);

            menu_items = gtk_menu_item_new_with_label("Next wallpaper");
            gtk_menu_shell_append(GTK_MENU_SHELL(mStatusIconMenu), menu_items);
            g_signal_connect_swapped(menu_items, "activate", G_CALLBACK(agent_action), (gpointer) g_strdup("signext"));
            gtk_widget_show(menu_items);

            menu_items = gtk_separator_menu_item_new();
            gtk_menu_shell_append (GTK_MENU_SHELL(mStatusIconMenu), menu_items);
            gtk_widget_show(menu_items);

            menu_items = gtk_menu_item_new_with_label("Quit");
            gtk_menu_shell_append (GTK_MENU_SHELL(mStatusIconMenu), menu_items);
            g_signal_connect_swapped(menu_items, "activate", G_CALLBACK(agent_action), (gpointer) g_strdup("sigquit"));
            gtk_widget_show(menu_items);

            // Create status icon
#ifdef LD_USE_APPINDICATOR
            gLogger->Info("This build has AppIndicator support");

            if (!mDisableAppIndicator)
            {
                gLogger->Info("You can disable AppIndicator with the --no-appindicator command line switch");
                mStatusIconAI = app_indicator_new(LD_APPNAME, (Filesystem::gWorkingDirectory / LD_ICON_PATH).c_str(),
                    APP_INDICATOR_CATEGORY_APPLICATION_STATUS);

                app_indicator_set_status(mStatusIconAI, APP_INDICATOR_STATUS_ACTIVE);
                app_indicator_set_menu(mStatusIconAI, GTK_MENU(mStatusIconMenu));
            }
            else
#endif // LD_USE_APPINDICATOR
            {
                gLogger->Info("Running without AppIndicator");

                if (DesktopEnvApi::GetDesktopEnv() == DesktopEnv::GNOME)
                {
                    gLogger->TraceWarn("On GNOME not using AppIndicator may result in a missing tray icon");
                    gLogger->TraceWarn("Try rebuilding the application with AppIndicator, or removing the --no-appindicator switch!");
                }

                mStatusIcon = gtk_status_icon_new_from_file(LD_ICON_PATH);

                g_signal_connect(G_OBJECT(mStatusIcon), "button_press_event",
                    G_CALLBACK(tray_icon_on_click), NULL);

                g_signal_connect(G_OBJECT(mStatusIcon), "popup-menu",
                    G_CALLBACK(tray_icon_on_menu), NULL);

                gtk_status_icon_set_tooltip_text(mStatusIcon, LD_APPNAME);
                gtk_status_icon_set_visible(mStatusIcon, TRUE);
            }
        }

        /* static */ void UIManager::CloseGracefully()
        {
            gLogger->Info("Called CloseGracefully - Shutting down!");

            CefQuitMessageLoop();
            gLogger->Info("Exited message loop");

            CefShutdown();
            gLogger->Info("Successfully destroyed CEF");

            if (mHostPID == 0)
            {
                exit(0);
                return;
            }

            if (getpid() == mHostPID)
            {
                // Clear the pid file if we are the host process
                Filesystem::PIDFileSet("cefapp.pid", 0);

                // This prevents sigsegv in CEF's AfterRun function
                // TODO: WHY ARE WE KILLING OURSELFS?
                kill(getpid(), SIGKILL);
                return;
            }

            kill(mHostPID, SIGINT); // We are a child process, so send an interrupt signal to the host process
        }

        /* static */ bool UIManager::SaveFileDialog(std::string filename, std::string current_name, std::string& res)
        {
            gLogger = Logging::LoggerManager::GetLogger("UIBase");
            gLogger->TraceDebug("Opening save file dialog with for %s", filename.c_str());

            nfdchar_t *outPath = nullptr;
            nfdresult_t result = NFD_SaveDialog(("*" + stdfs::path{current_name}.extension().string()).c_str(), filename.c_str(), &outPath);

            if (result == NFD_OKAY)
            {
                res = {outPath};
                free(outPath);
                return true;
            }
            else if (result != NFD_CANCEL)
                gLogger->TraceError("%s", NFD_GetError());

            return false;
        }

        /* static */ void UIManager::ShowDownloadFinishedDialog()
        {
            GtkWidget* dialog;

            dialog = gtk_message_dialog_new(
                nullptr,
                GTK_DIALOG_DESTROY_WITH_PARENT,
                GTK_MESSAGE_INFO,
                GTK_BUTTONS_CLOSE,
                "Download successed!");

            gtk_dialog_run(GTK_DIALOG(dialog));
            gtk_widget_destroy(dialog);
        }
    }
}
