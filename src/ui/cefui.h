
#ifndef LD_CEFUI_H
#define LD_CEFUI_H

#include "uibase.h"
#include "filesystem.h"

#include "include/views/cef_browser_view.h"
#include "include/views/cef_window.h"

namespace LeagueDisplays
{
    namespace UI
    {
        namespace CEF
        {
            class WindowDelegate : public CefWindowDelegate
            {
                public:
                    explicit WindowDelegate(CefRefPtr<CefBrowserView> browser_view)
                        : mBrowserView(browser_view) { }

                    void OnWindowCreated(CefRefPtr<CefWindow> window) OVERRIDE;
                    void OnWindowDestroyed(CefRefPtr<CefWindow> window) OVERRIDE;

                    bool CanClose(CefRefPtr<CefWindow> window) OVERRIDE;

                    CefSize GetPreferredSize(CefRefPtr<CefView> view) OVERRIDE;

                    bool CanResize(CefRefPtr<CefWindow> window) OVERRIDE { return false; }
                    bool CanMaximize(CefRefPtr<CefWindow> window) OVERRIDE { return false; }

                private:
                    CefRefPtr<CefBrowserView> mBrowserView;

                    IMPLEMENT_REFCOUNTING(WindowDelegate);
                    DISALLOW_COPY_AND_ASSIGN(WindowDelegate);
            };

            class BrowserViewDelegate : public CefBrowserViewDelegate
            {
                public:
                    BrowserViewDelegate() {}

                    bool OnPopupBrowserViewCreated(CefRefPtr<CefBrowserView> browser_view,
                        CefRefPtr<CefBrowserView> popup_browser_view,
                        bool is_devtools);

                private:
                    IMPLEMENT_REFCOUNTING(BrowserViewDelegate);
                    DISALLOW_COPY_AND_ASSIGN(BrowserViewDelegate);
            };
        }
    }
}

#endif
