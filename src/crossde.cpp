
#include "crossde.h"
#include "log.h"

#include <stdio.h>
#include <sstream>
#include <cstring>

#ifdef LOGGING_SOURCE
#undef LOGGING_SOURCE
#endif // LOGGING_SOURCE

#define LOGGING_SOURCE "DesktopEnvApi"

namespace LeagueDisplays {
    /*static*/ void DesktopEnvApi::LogDesktopEnv()
    {
        auto logger = Logging::LoggerManager::GetLogger("DesktopEnvApi");

        const char* xdg_env = getenv("XDG_CURRENT_DESKTOP");

        if (xdg_env)
            logger->Info("XDG_CURRENT_DESKTOP is present, value is %s", xdg_env);

        switch (GetDesktopEnv()) {
            case DesktopEnv::DEEPIN:
                logger->Info("Detected desktop: $$lDeepin");
                break;
            case DesktopEnv::KDE_PLASMA:
                logger->Info("Detected desktop: $$lKDE plasma");
                break;
            case DesktopEnv::GNOME:
                logger->Info("Detected desktop: $$lGNOME");
                break;
            case DesktopEnv::LXDE:
                logger->Info("Detected desktop: $$lLXDE");
                break;
            case DesktopEnv::LXQT:
                logger->Info("Detected desktop: $$lLXQT");
                break;
            case DesktopEnv::XFCE4:
                logger->Info("Detected desktop: $$lXFCE 4");
                break;
            case DesktopEnv::CINNAMON:
                logger->Info("Detected desktop: $$lCINNAMON");
                break;
            case DesktopEnv::UNITY:
                logger->Info("Detected desktop: $$lUNITY");
                break;
            case DesktopEnv::MATE:
                logger->Info("Detected desktop: $$lMATE");
                break;
            case DesktopEnv::I3:
                logger->Info("Detected desktop: $$li3");
                break;
            default:
                logger->TraceWarn("Unknown desktop environment");
                break;
        }
    }

    /*static*/ DesktopEnv DesktopEnvApi::GetDesktopEnv()
    {
        const char* xdg_env = getenv("XDG_CURRENT_DESKTOP");

        if (xdg_env) {
            if (strstr(xdg_env, "unity") || strstr(xdg_env, "Unity"))
                return DesktopEnv::UNITY;

            if (strstr(xdg_env, "GNOME"))
                return DesktopEnv::GNOME;
        }

        if (CheckIfProcessIsRunning("startdde"))
            return DesktopEnv::DEEPIN;

        if (CheckIfProcessIsRunning("plasmashell") || CheckIfProcessIsRunning("plasma-desktop"))
            return DesktopEnv::KDE_PLASMA;

        if (CheckIfProcessIsRunning("i3"))
            return DesktopEnv::I3;

        if (CheckIfProcessIsRunning("gnome-panel") && !strcmp(getenv("DESKTOP_SESSION"), "gnome"))
            return DesktopEnv::GNOME;

        if (CheckIfProcessIsRunning("xfce4-panel"))
            return DesktopEnv::XFCE4;

        if (CheckIfProcessIsRunning("mate-panel") || CheckIfProcessIsRunning("mate-session"))
            return DesktopEnv::MATE;

        if (CheckIfProcessIsRunning("lxqt-session"))
            return DesktopEnv::LXQT;

        if (CheckIfProcessIsRunning("lxpanel"))
            return DesktopEnv::LXDE;

        if (CheckIfProcessIsRunning("cinnamon"))
            return DesktopEnv::CINNAMON;

        if (CheckIfProcessIsRunning("unity-settings"))
            return DesktopEnv::UNITY;

        return DesktopEnv::UNKNOWN;
    }

    /*static*/ void DesktopEnvApi::ChangeBackground(std::string path)
    {
        auto logger = Logging::LoggerManager::GetLogger("DesktopEnvApi");

        std::ostringstream cmdstream;

        switch (GetDesktopEnv()) {
            case DesktopEnv::DEEPIN:
                cmdstream << "gsettings set com.deepin.wrap.gnome.desktop.background picture-uri \"file://" << path << "\"";
                break;
            case DesktopEnv::KDE_PLASMA:
                // 1) Why this has to be this complicated
                // 2) Thanks random redditor from r/kde
                cmdstream << "dbus-send --session --dest=org.kde.plasmashell --type=method_call /PlasmaShell org.kde.PlasmaShell.evaluateScript 'string:"
                    << "var Desktops = desktops();"
                    << "for (i=0;i<Desktops.length;i++) {"
                    <<  "d = Desktops[i];"
                    <<  "d.wallpaperPlugin = \"org.kde.image\";"
                    <<  "d.currentConfigGroup = Array(\"Wallpaper\", \"org.kde.image\", \"General\");"
                    <<  "d.writeConfig(\"Image\", \"" << path << "\");"
                    << "}'";
                break;
            case DesktopEnv::GNOME:
                if (CheckIfCommandExists("gconftool-2"))
                    cmdstream << "gconftool-2 --type=string --set /desktop/gnome/background/picture_filename \"" << path << "\"";
                else
                    cmdstream << "gsettings set org.gnome.desktop.background picture-uri \"file://" << path << "\"";

                break;
            case DesktopEnv::LXDE:
                cmdstream << "pcmanfm --set-wallpaper \"" << path << "\"";
                break;
            case DesktopEnv::LXQT:
                cmdstream << "pcmanfm-qt --set-wallpaper \"" << path << "\"";
                break;
            case DesktopEnv::XFCE4:
                cmdstream << "xfce4-set-wallpaper \"" << path << "\"";
                break;
            case DesktopEnv::CINNAMON:
                cmdstream << "gsettings set org.cinnamon.desktop.background picture-uri \"file://" << path << "\"";
                break;
            case DesktopEnv::UNITY:
                cmdstream << "gsettings set org.gnome.desktop.background picture-uri \"file://" << path << "\"";
                break;
            case DesktopEnv::MATE:
                cmdstream << "gsettings set org.mate.caja.preferences background-uri \"file://" << path << "\";";
                cmdstream << "gsettings set org.mate.background picture-filename \"" << path << "\"";
                break;
            case DesktopEnv::I3:
                cmdstream << "feh --bg-scale \"" << path << "\";";
                break;
            default:
                logger->TraceWarn("Unknown desktop environment, using GNOMEShell\n");
                cmdstream << "gsettings set org.gnome.desktop.background picture-uri \"file://" << path << "\"";
                break;
        }

        logger->Info("Setting wallpaper: %s\n    command: %s", path.c_str(), cmdstream.str().c_str());

        if (system(cmdstream.str().c_str()) != 0)
            logger->TraceError("Failed to execute command for changing the wallpaper");
    }

    /*static*/ void DesktopEnvApi::OpenUrlInDefaultBrowser(std::string url)
    {
        auto logger = Logging::LoggerManager::GetLogger("DesktopEnvApi");

        std::string cmd = "xdg-open \"" + url + "\"";
        logger->Info("CMD: %s", cmd.c_str());
        system(cmd.c_str());
    }

    /*static*/ bool DesktopEnvApi::CheckIfProcessIsRunning(std::string name)
    {
        auto logger = Logging::LoggerManager::GetLogger("DesktopEnvApi");

        char cmd[1024];

        // Gets the number of lines from the process list that match the given process name
        snprintf(cmd, 1024, "ps -A | grep '%s' | wc -l", name.c_str());

        FILE* fp = popen(cmd, "r");

        if (!fp)
        {
            logger->TraceError("Error popen with %s\n", cmd);
            abort();
            return false;
        }

        int ch = fgetc(fp);
        pclose(fp);

        return (ch != EOF && ch != '0');
    }

    /*static*/ bool DesktopEnvApi::CheckIfCommandExists(std::string name)
    {
        auto logger = Logging::LoggerManager::GetLogger("DesktopEnvApi");

        char cmd[1024];

        snprintf(cmd, 1024, "which '%s' &> /dev/null", name.c_str());

        FILE* fp = popen(cmd, "r");

        if (!fp)
        {
            logger->TraceError("Error popen with %s\n", cmd);
            abort();
            return false;
        }

        int ch = fgetc(fp);
        pclose(fp);

        return ch != EOF;
    }
}
