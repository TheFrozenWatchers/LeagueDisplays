
#include <unistd.h>

#include "dispatcher.h"

int main(int argc, char *argv[])
{
    using namespace LeagueDisplays;

    bool log2file = false, nologger = false;
    for (int i = 0; i < argc; i++)
    {
        if (strcmp(argv[i], "--ld-log-to-file") == 0)
            log2file = true;
        else if (strcmp(argv[i], "--no-logger") == 0)
            nologger = true;
    }

    if (!nologger)
        Logging::LoggerManager::Initialize(log2file);

    auto& dp = StartupDispatcher::Initialize(argc, argv);
    int runres = dp.Run();

    usleep(100000);

    return runres;
}
