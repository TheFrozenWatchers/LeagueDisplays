
#include "dispatcher.h"
#include "filesystem.h"
#include "crossde.h"
#include "misc.hpp"
#include "cef/client_app.h"
#include "ui/uibase.h"
#include "screensaver/xscreensaver.h"
#include "background_daemon.h"

#include <unistd.h>
#include <signal.h>

namespace LeagueDisplays
{
    /* static */ std::unique_ptr<StartupDispatcher> StartupDispatcher::gInstance;

    /* static */ StartupDispatcher& StartupDispatcher::Initialize(int argc, char *argv[])
    {
        gInstance = std::unique_ptr<StartupDispatcher>{new StartupDispatcher{argc, argv}};

        return *gInstance;
    }

    int StartupDispatcher::Run()
    {
        if (mCmdline->HasSwitch("ld-log-level"))
        {
            auto level = Logging::LL_DEFAULT;

            switch (Misc::fnvHash(mCmdline->GetSwitchValue("ld-log-level")))
            {
                case "debug"_fnv: level = Logging::LL_DEBUG; break;
                case "info"_fnv:  level = Logging::LL_INFO;  break;
                case "fixme"_fnv: level = Logging::LL_FIXME; break;
                case "warn"_fnv:  level = Logging::LL_WARN;  break;
                case "error"_fnv: level = Logging::LL_ERROR; break;
                default:
                    mLogger->TraceError("Invalid log level \"%s\", expected (debug|info|fixme|warn|error)",
                        mCmdline->GetSwitchValue("ld-log-level").ToString().c_str());
                    return 1;
            }

            Logging::LoggerManager::Instance().SetMinimumLogLevel(level);
        }

        // alias for --ld-log-level=error
        if (mCmdline->HasSwitch("silent-logging"))
            Logging::LoggerManager::Instance().SetMinimumLogLevel(Logging::LL_ERROR);

        #ifdef LD_USE_APPINDICATOR
        UI::UIManager::mDisableAppIndicator = mCmdline->HasSwitch("no-appindicator");
        #endif

        gInstance->mLogger->Debug("-- Command line dump start ---");

        for (int i = 1; i < mMainArgs.argc; i++)
            gInstance->mLogger->Debug(" @%02d  %s", i, mMainArgs.argv[i]);

        gInstance->mLogger->Debug("-- Command line dump end ---");

        Filesystem::Initialize();

        if (mCmdline->HasSwitch("get-desktop-env"))
        {
            DesktopEnvApi::LogDesktopEnv();
            return 0;
        }

        RegisterRunners();

        if (mCmdline->HasSwitch("ld-fork"))
            return RunForked();
        else
            return RunMain();
    }

    StartupDispatcher::StartupDispatcher(int argc, char *argv[])
        : StartupContext({argc, argv}), Logging::LoggerHolder{"Startup"} { }

    int StartupDispatcher::RunForked()
    {
        mLogger->TraceDebug("Running in daemon mode");

        pid_t pid = fork();

        if (pid == 0)
            return RunMain();

        return 0;
    }

    int StartupDispatcher::RunMain()
    {
        mLogger->TraceDebug("I'm the main function!");

        InitUIBasics();

        std::string mode = "launch";

        // --tray is an alias for --ld-mode=agent
        if (mCmdline->HasSwitch("tray"))
            mode = "agent";
        else if (mCmdline->HasSwitch("ld-mode"))
        {
            mode = mCmdline->GetSwitchValue("ld-mode");
            mLogger->TraceDebug("--ld-mode switch is present, value was %s hash is %08x", mode.c_str(), Misc::fnvHash(mode));
        }

        mLogger->TraceDebug("There are %d runners to chose from", mRunners.size());

        auto it = mRunners.find(Misc::fnvHash(mode));

        if (it == mRunners.end())
        {
            mLogger->TraceError("No runner for mode <%s>", mode.c_str());
            return 1;
        }

        return it->second->Run(*this);
    }

    void StartupDispatcher::InitUIBasics()
    {
        if (!mCmdline->HasSwitch("ld-host-pid"))
            UI::UIManager::mHostPID = getpid();

        if (mCmdline->HasSwitch("allow-multiple-instances"))
            UI::UIManager::mAllowMultipleInstances = true;

        UI::XScreensaver::CreateLaunchScript();
        UI::XScreensaver::ModifyXscreensaverFile();

        UI::UIManager::RegisterSignalHandlers();
    }
}
