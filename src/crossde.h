
#ifndef LD_CROSS_DE_H
#define LD_CROSS_DE_H

#include <string>

namespace LeagueDisplays
{
    enum class DesktopEnv
    {
        DEEPIN,
        KDE_PLASMA,
        GNOME,
        XFCE4,
        MATE,
        LXDE,
        LXQT,
        CINNAMON,
        UNITY,
        I3,
        UNKNOWN = -1
    };

    class DesktopEnvApi
    {
        public:
            static void LogDesktopEnv();
            static DesktopEnv GetDesktopEnv();
            static void ChangeBackground(std::string path);
            static void OpenUrlInDefaultBrowser(std::string url);

        private:
            static bool CheckIfProcessIsRunning(std::string name);
            static bool CheckIfCommandExists(std::string name);
    };
}

#endif
