
#include "background_daemon.h"
#include "log.h"
#include "filesystem.h"
#include "crossde.h"
#include "appconfig.h"
#include "misc.hpp"

#include <unistd.h>

namespace LeagueDisplays
{
    /* static */ pthread_t BackgroundDaemon::mBackgroundChangerThread;
    /* static */ int64_t BackgroundDaemon::mNextBackgroundChangeTime = 0;
    /* static */ std::shared_ptr<Logging::Logger> gLogger;

    /* static */ void BackgroundDaemon::Start()
    {
        gLogger = Logging::LoggerManager::GetLogger("BackgroundDaemon");

        gLogger->Info("Starting daemon service...");

        auto cfg = AppConfig::Acquire();
        cfg->LoadSettings();
        cfg->LoadPlaylist();

        pthread_create(&mBackgroundChangerThread, nullptr, WorkerThreadProc, nullptr);
    }

    /* static */ void BackgroundDaemon::Stop()
    {
        gLogger->Info("Stopping daemon service...");

        pthread_cancel(mBackgroundChangerThread);
    }

    /* static */ void BackgroundDaemon::Next()
    {
        gLogger->Info("Displaying the next wallpaper on the playlist");
        mNextBackgroundChangeTime = 0;
    }

    /* static */ void BackgroundDaemon::SetAutostartEnabled(bool flag)
    {
        auto autostart_dir = Filesystem::gUserHome / ".config/autostart/";

        if (!stdfs::exists(autostart_dir))
            stdfs::create_directories(autostart_dir);

        if (!flag)
        {
            stdfs::remove(autostart_dir / "leaguedisplaysagent.desktop");
            gLogger->Info("Removed agent from autostart");
        }
        else
        {
            Filesystem::WriteAllText(autostart_dir / "leaguedisplaysagent.desktop",
                "[Desktop Entry]\n"
                "Version=1.0\n"
                "Name=LeagueDisplays Agent\n"
                "Comment=Start LeagueDisplay's agent\n"
                "Exec=\"" + Filesystem::gLauncherPath.string() + "\" --tray --ld-fork\n"
                "Terminal=false\n"
                "Type=Application\n"
            );

            gLogger->Info("Added agent to autostart");
        }
    }

    /* static */ bool BackgroundDaemon::IsAutostartEnabled()
    {
        return stdfs::exists(Filesystem::gUserHome / ".config/autostart/leaguedisplaysagent.desktop");
    }

    /* static */ void* BackgroundDaemon::WorkerThreadProc(void* arg)
    {
        gLogger->Info("Created background changer thread[%08x]!", pthread_self());

        int wallpaper_index = 0;
        unsigned int current_wp_hash = 0;

        auto base_path = Filesystem::gFilesystemCacheDir / "LeagueScreensaver/content-cache/asset/";

        while (true)
        {
            auto cfg = AppConfig::Acquire();

            cfg->ReloadIfRequested();

            if (cfg->BackgroundPlaylistChanged())
            {
                gLogger->TraceDebug("Playlist changed!");
                wallpaper_index = -1;

                if (current_wp_hash)
                {
                    int i = 0;
                    for (std::string s : cfg->BackgroundPlaylist())
                    {
                        if (current_wp_hash == Misc::fnvHash(s))
                        {
                            gLogger->TraceDebug("Current background is still in the playlist, keeping it!");
                            wallpaper_index = i;
                            break;
                        }

                        i++;
                    }
                }

                if (wallpaper_index == -1)
                {
                    wallpaper_index = 0;
                    mNextBackgroundChangeTime = 0;
                }
            }

            if (cfg->BackgroundPlaylist().size() > 0 && mNextBackgroundChangeTime <= time(nullptr))
            {
                std::string wp = cfg->BackgroundPlaylist().at(wallpaper_index++);

                if (wallpaper_index >= cfg->BackgroundPlaylist().size())
                    wallpaper_index = 0;

                unsigned int hash = Misc::fnvHash(wp);

                if (hash != current_wp_hash)
                {
                    auto path = base_path / ("c-o-" + wp);

                    if (stdfs::exists(path.replace_extension(".jpg")));
                    else if (stdfs::exists(path.replace_extension(".png")));
                    else
                    {
                        gLogger->TraceError("Could not find file for wallpaper: %s", wp.c_str());
                        goto _end;
                    }

                    DesktopEnvApi::ChangeBackground(path);
                }

                current_wp_hash = hash;

                _end:
                mNextBackgroundChangeTime = time(nullptr) + cfg->Settings()->wp_imageDuration;
            }

            cfg.Release();

            usleep(250 * 1000);
        }

        return nullptr;
    }
}
