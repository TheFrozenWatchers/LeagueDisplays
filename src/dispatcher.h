
#ifndef LD_DISPATCHER_H
#define LD_DISPATCHER_H

#include <memory>
#include <map>

#include "include/cef_app.h"

#include "misc.hpp"
#include "log.h"

namespace LeagueDisplays
{
    struct StartupContext
    {
        CefRefPtr<CefCommandLine>       mCmdline;
        CefMainArgs                     mMainArgs;

        StartupContext(int argc, char *argv[])
            : mCmdline{CefCommandLine::CreateCommandLine()}, mMainArgs{{argc, argv}}
        {
            mCmdline->InitFromArgv(argc, argv);
        }
    };

    struct StartupRunnerBase
    {
        virtual ~StartupRunnerBase() = default;
        virtual int Run(const StartupContext&) = 0;
    };

    template <typename TMode>
    struct StartupRunner : StartupRunnerBase, protected Logging::LoggerHolder
    {
        using gMode = TMode;

        static constexpr unsigned int gHash = gMode::hash;

        StartupRunner() : Logging::LoggerHolder{Misc::const_concat("Startup ~ ", gMode::value).data()} { }
    };

    class StartupDispatcher final : public StartupContext, public Logging::LoggerHolder
    {
        public:
            static StartupDispatcher& Initialize(int argc, char *argv[]);
            int Run();

            template<typename T>
            void Register() {
                mLogger->TraceDebug("Registering runner for mode <%s> hash is <%08x>", T::gMode::value, T::gHash);
                mRunners.insert({T::gHash, std::make_shared<T>()});
            }

            static inline StartupDispatcher& Instance() noexcept { return *gInstance; }
        private:
            StartupDispatcher(int argc, char *argv[]);

            void ReadExePath();
            int RunForked();
            int RunMain();

            void RegisterRunners();

            void InitUIBasics();

            static std::unique_ptr<StartupDispatcher>   gInstance;

            std::map<unsigned int, std::shared_ptr<StartupRunnerBase>> mRunners;
    };
}

#endif
