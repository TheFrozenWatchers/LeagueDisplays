
#include <string>
#include <fstream>
#include <vector>

#include "scheme_handlers.h"
#include "filesystem.h"
#include "appconfig.h"
#include "misc.hpp"
#include "background_daemon.h"
#include "screensaver/xscreensaver_config.h"

namespace LeagueDisplays
{
    static inline void UpdateScreensaverStuff()
    {
        auto logger = Logging::LoggerManager::GetLogger("ThemesRes");

        auto base_path = Filesystem::gFilesystemCacheDir / "LeagueScreensaver/content-cache/asset/";
        auto dst_path = Filesystem::gFilesystemCacheDir / "screensaver/";

        stdfs::create_directories(dst_path / "playlist/");
        
        for (const auto& dirEntry : stdfs::recursive_directory_iterator(dst_path / "playlist/"))
            if (dirEntry.is_symlink())
                stdfs::remove(dirEntry.path());

        std::ofstream ofs{dst_path / "playlist.js"};

        assert(ofs);

        ofs << "// Automatically generated file, DO NOT EDIT!" << std::endl;

        auto cfg = AppConfig::Acquire();
        std::vector<std::string> playlist = cfg->ScreensaverPlaylist();

        if (playlist.size() > 0)
        {
            ofs << "PLAYLIST.custom = {" << std::endl;
            ofs << "  assets: [" << std::endl;

            for (std::string s : playlist)
            {
                auto path = base_path / ("c-o-" + s);

                if (stdfs::exists(path.replace_extension(".jpg")));
                else if (stdfs::exists(path.replace_extension(".png")));
                else if (stdfs::exists(path.replace_extension(".webm")));
                else if (stdfs::exists(path.replace_extension(".mp4")));
                else
                {
                    logger->TraceError("Could not find file for screensaver asset: %s", s.c_str());
                    continue;
                }

                ofs << "    'playlist/" << path.filename().string() << "'," << std::endl;

                stdfs::create_symlink(path, dst_path / "playlist" / path.filename());
            }

            ofs << "  ]" << std::endl << "};" << std::endl;
        }

        ofs.close();
    }

    namespace CEF
    {
        bool ThemesResourceHandler::ProcessRequest(CefRefPtr<CefRequest> request, CefRefPtr<CefCallback> callback)
        {
            std::string url = request->GetURL().ToString();
            std::string method = request->GetMethod().ToString();
            mLogger->TraceDebug("Processing request (%s %s)", method.c_str(), url.c_str());

            auto splitted = Misc::split(url, '/');
            for (int i = 0; i < 3; i++)
                splitted.erase(splitted.begin());

            unsigned int service = Misc::fnvHash(splitted.size() > 0 ? splitted[0].c_str() : "");
            unsigned int call    = Misc::fnvHash(splitted.size() > 1 ? (method + ":" + splitted[1]).c_str() : "");

            mLogger->TraceDebug("API call: %s/%s [%08lx%08lx]",
                splitted.size() > 0 ? splitted[0].c_str() : "",
                splitted.size() > 1 ? (method + ":" + splitted[1]).c_str() : "",
                service, call);

            std::string post_data_s;

            if (method == "PUT" || method == "POST")
            {
                CefRefPtr<CefPostData> post_data = request->GetPostData();
                if (post_data)
                {
                    CefPostData::ElementVector chunks;
                    post_data->GetElements(chunks);

                    for (CefRefPtr<CefPostDataElement> chunk : chunks)
                    {
                        size_t chunksize = chunk->GetBytesCount();

                        std::unique_ptr<char[]> buffer{new char[chunksize]};

                        assert(chunk->GetBytes(chunksize, buffer.get()) == chunksize);

                        post_data_s += std::string{buffer.get(), chunksize};
                    }

                    chunks.clear();
                }
            }

            // mLogger->TraceDebug("Post data: %s", post_data_s.c_str());

            rapidjson::Document d;

            switch (service)
            {
                case "screensaver"_fnv:
                {
                    switch (call)
                    {
                        case "GET:settings"_fnv:
                        {
                            int timeout = 0;

                            XScreensaverConfig cfg{Filesystem::gUserHome / ".xscreensaver"};
                            std::string value = cfg.HasKey("timeout") ? cfg.GetKeyValue("timeout") : "00:01:00";

                            auto pos = value.find(':');
                            timeout += atoi(value.substr(0, pos).c_str()) * 60 * 60;

                            value = value.substr(pos + 1);
                            pos = value.find(':');
                            timeout += atoi(value.substr(0, pos).c_str()) * 60;

                            value = value.substr(pos + 1);
                            timeout += atoi(value.c_str());

                            mStream << "{\"screensaver_timeout\":" << timeout << "}\n";
                            mErrorCode = 200;
                            callback->Continue();
                            return true;
                        }

                        case "PUT:settings"_fnv:
                        {
                            if (splitted.size() <= 2)
                            {
                                d.Parse(post_data_s.c_str());

                                auto cfg = AppConfig::Acquire();

                                for (auto iter = d.MemberBegin(); iter != d.MemberEnd(); ++iter)
                                    cfg->ModifyOption(std::string(iter->name.GetString()), iter->value, ConfigType::SCREENSAVER);
                            }
                            else
                            {
                                if (splitted[2] == "screensaver-timeout")
                                {
                                    int timeout = atoi(post_data_s.c_str());
                                    std::stringstream ss;
                                    ss << timeout / 60 / 60;
                                    ss << ":";
                                    ss << timeout / 60 - ((timeout / 60 / 60) * 60);
                                    ss << ":";
                                    ss << timeout - ((timeout / 60 - ((timeout / 60 / 60) * 60)) * 60) - ((timeout / 60 / 60) * 60 * 60);

                                    XScreensaverConfig cfg{Filesystem::gUserHome / ".xscreensaver"};
                                    cfg.SetKeyValue("timeout", ss.str());
                                    cfg.Save();

                                    break;
                                }

                                mLogger->TraceWarn("partial screensaver/PutSettings is not implemented yet!");
                            }
                            break;
                        }

                        case "POST:playlist"_fnv:
                        {
                            AppConfig::Acquire()->LoadPlaylist();

                            mErrorCode = 1200;
                            callback->Continue();
                            return true;
                        }

                        case "PUT:activation"_fnv:
                        {
                            UpdateScreensaverStuff();
                            mErrorCode = 1200;
                            callback->Continue();
                            return true;
                        }

                        default:
                            mLogger->TraceWarn("Unhandled call %08lx", call);
                            break;
                    }

                    break;
                }
                case "wallpaper"_fnv:
                {
                    switch (call)
                    {
                        case "GET:settings"_fnv:
                            mLogger->TraceWarn("wallpaper/GetSettings is not implemented yet!");
                            break;

                        case "PUT:settings"_fnv:
                        {
                            if (splitted.size() <= 2)
                            {
                                d.Parse(post_data_s.c_str());

                                {
                                    auto cfg = AppConfig::Acquire();

                                    for (auto iter = d.MemberBegin(); iter != d.MemberEnd(); ++iter)
                                    {
                                        const rapidjson::Value& v = iter->value;
                                        std::string key = iter->name.GetString();

                                        if (key == "rotate-frequency")
                                        {
                                            cfg->ModifyOptionS("wp_imageDuration", v, ConfigType::GENERAL);
                                        }
                                        else
                                            cfg->ModifyOption(std::string(iter->name.GetString()), v, ConfigType::WALLPAPER);
                                    }
                                }

                                mErrorCode = 1200;
                                callback->Continue();
                                return true;
                            }
                            else
                            {
                                mLogger->TraceWarn("partial wallpaper/PutSettings is not implemented yet!");
                            }

                            break;
                        }

                        case "POST:playlist"_fnv:
                        {
                            mLogger->Info("Playlist reload requested");

                            AppConfig::Acquire()->LoadPlaylist();

                            mErrorCode = 1200;
                            callback->Continue();
                            return true;
                        }
                        default:
                            mLogger->TraceWarn("Unhandled call %08lx", call);
                            break;
                    }

                    break;
                }

                case "assistant"_fnv:
                {
                    switch (call)
                    {
                        case "GET:settings"_fnv:
                            mStream << "{\"enable-assistant-startup\":"
                                << (BackgroundDaemon::IsAutostartEnabled() ? "true" : "false") << "}\n";

                            mErrorCode = 200;
                            callback->Continue();
                            return true;

                        case "PUT:settings"_fnv:
                            d.Parse(post_data_s.c_str());

                            if (d.HasMember("enable-assistant-startup"))
                                BackgroundDaemon::SetAutostartEnabled(d["enable-assistant-startup"].GetBool());

                            break;
                        default:
                            mLogger->TraceWarn("Unhandled call %08lx", call);
                            break;
                    }

                    break;
                }

                case "powersettings"_fnv:
                {
                    mLogger->TraceWarn("Power-management settings not implemented yet!");
                    break;
                }

                default:
                    mLogger->TraceWarn("Unhandled service %08lx", service);
                    break;
            }

            return false;
        }

        void ThemesResourceHandler::GetResponseHeaders(CefRefPtr<CefResponse> response, int64& response_length, CefString& redirectUrl)
        {
            CefRequest::HeaderMap headers;
            response->GetHeaderMap(headers);
            headers.insert({ "Access-Control-Allow-Origin", "*" });
            response->SetHeaderMap(headers);

            response->SetMimeType("application/json");

            if (mErrorCode > 1000)
            {
                response->SetStatus(mErrorCode - 1000);
                response_length = 0;
            }
            else
            {
                response->SetStatus(mErrorCode);
                mStream.seekg(0, std::ios::end);
                response_length = mStream.tellg();
                mStream.seekg(0);
            }

            mResponseLength = response_length;
            mLogger->TraceDebug("Code: %d, Response length: %d", mErrorCode, response_length);
        }

        bool ThemesResourceHandler::Skip(int64 bytes_to_skip, int64& bytes_skipped, CefRefPtr<CefResourceSkipCallback> callback)
        {
            auto pos = mStream.tellg();
            mStream.seekg(bytes_to_skip, std::ios::cur);
            bytes_skipped = mStream.tellg() - pos;

            return true;
        }

        bool ThemesResourceHandler::Read(void* data_out, int bytes_to_read, int& bytes_read, CefRefPtr<CefResourceReadCallback> callback)
        {            
            bytes_read = mStream.readsome((char*)data_out, bytes_to_read);
            return bytes_read > 0;
        }

        void ThemesResourceHandler::Cancel()
        {
            mStream.clear();
        }
    }
}
