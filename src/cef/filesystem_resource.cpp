
#include <fstream>
#include <cstring>

#include "filesystem.h"
#include "scheme_handlers.h"
#include "appconfig.h"
#include "misc.hpp"

#include "include/cef_request.h"
#include "include/cef_parser.h"
#include "include/wrapper/cef_helpers.h"

namespace LeagueDisplays
{
    namespace CEF
    {
        bool FilesystemResourceHandler::Open(CefRefPtr<CefRequest> request, bool& handle_request, CefRefPtr<CefCallback> callback)
        {
            std::string url = request->GetURL().ToString();
            std::string method = request->GetMethod().ToString();

            mLogger->TraceDebug("Processing request (%s %s)", method.c_str(), url.c_str());

            auto path = Filesystem::gFilesystemCacheDir / url.substr(strlen("http://filesystem/") + !url.find("https"));

            mMimeType = "text/plain";
            mReadOffset = 0;

            if (method == "POST")
            {
                Filesystem::FileMkdirs(path);

                mErrorCode = 200;
                mResponseLength = 0;

                CefRefPtr<CefPostData> post_data = request->GetPostData();
                if (post_data)
                {
                    CefPostData::ElementVector file_chunks;
                    post_data->GetElements(file_chunks);

                    std::ofstream ofs(path, std::ios::out | std::ios::binary);

                    for (CefRefPtr<CefPostDataElement> chunk : file_chunks)
                    {
                        size_t chunksize = chunk->GetBytesCount();

                        std::unique_ptr<unsigned char[]> buffer{new unsigned char[chunksize]};

                        size_t getbytes_res = chunk->GetBytes(chunksize, buffer.get());

                        if (getbytes_res != chunksize)
                        {
                            mLogger->TraceError("GetBytes returned %lu while chunksize is %lu", getbytes_res, chunksize);
                            abort();
                        }

                        ofs.write((char*)buffer.get(), chunksize);
                    }

                    size_t total_size = ofs.tellp();
                    ofs.close();

                    mLogger->TraceInfo("Saved file %s [%ld bytes]", path.c_str(), total_size);
                }
                else
                {
                    Filesystem::WriteAllText(path, {});
                    mLogger->TraceInfo("Saved empty file %s", path.c_str());
                }

                if (path == Filesystem::gSettingsFilePath)
                {
                    auto cfg = AppConfig::Acquire();
                    cfg->LoadSettings();
                }

                callback->Continue();
            }
            else if (method == "GET")
            {
                if (!stdfs::exists(path))
                {
                    mErrorCode = 404;
                    mResponseLength = 0;

                    mLogger->TraceWarn("File not found: %s", path.c_str());
                }
                else
                {
                    mErrorCode = 200;

                    if (mFileStream.is_open())
                    {
                        mLogger->TraceWarn("File stream is open when opening the new one");
                        mLogger->TraceWarn(" - Current offset is %u, file length is %u", mFileStream.tellg(), mResponseLength);
                        mFileStream.close();
                    }

                    FigureOutMimeType(path);
                    /*mFileStream.open(path, std::ios::in | std::ios::binary);
                    mFileStream.unsetf(std::ios::skipws);
                    mFileStream.seekg(0, std::ios_base::end);
                    mResponseLength = mFileStream.tellg();
                    mFileStream.seekg(0, std::ios_base::beg);*/
                    mFilePath = path;
                    mResponseLength = stdfs::file_size(mFilePath);
                }

                callback->Continue();
            }
            else if (method == "OPTIONS")
            {
                mResponseLength = 0;
                mErrorCode = stdfs::exists(path) ? 200 : 404;
                callback->Continue();
            }
            else
            {
                mLogger->TraceError("Unhandled method: %s - FATAL", method.c_str());

                CefRequest::HeaderMap headers;
                request->GetHeaderMap(headers);

                for (std::pair<CefString, CefString> q : headers)
                    mLogger->TraceError("%s: %s", q.first.ToString().c_str(), q.second.ToString().c_str());

                abort();
            }

            handle_request = true;
            return true;
        }

        void FilesystemResourceHandler::FigureOutMimeType(stdfs::path path)
        {
            auto ext = path.extension();

            switch (Misc::fnvHash(ext))
            {
                case ".jpg"_fnv:
                case ".jpeg"_fnv:
                    mMimeType = "image/jpeg";
                    break;
                case ".png"_fnv:
                    mMimeType = "image/png";
                    break;
                case ".webm"_fnv:
                    mMimeType = "video/webm";
                    break;
                case ".webp"_fnv:
                    mMimeType = "image/webp";
                    break;
                case ".weba"_fnv:
                    mMimeType = "audio/webm";
                    break;
                case ".mp3"_fnv:
                    mMimeType = "audio/mpeg";
                    break;
                case ".ogg"_fnv:
                    mMimeType = "audio/ogg";
                    break;
                case ".mp4"_fnv:
                    mMimeType = "video/mpeg";
                    break;
                case ".js"_fnv:
                    mMimeType = "text/javascript";
                    break;
                case ".json"_fnv:
                    mMimeType = "application/json";
                    break;
                case ".html"_fnv:
                case ".htm"_fnv:
                    mMimeType = "text/html";
                    break;
                case ".css"_fnv:
                    mMimeType = "text/css";
                    break;
                case ".txt"_fnv:
                    mMimeType = "text/plain";
                    break;
                default:
                    mMimeType = "application/octet-stream";
                    break;
            }
        }

        void FilesystemResourceHandler::GetResponseHeaders(CefRefPtr<CefResponse> response, int64& response_length, CefString& redirectUrl)
        {
            CefRequest::HeaderMap headers;
            response->GetHeaderMap(headers);
            headers.insert({ "Access-Control-Allow-Origin", "*" });
            response->SetHeaderMap(headers);

            response->SetMimeType(mMimeType);
            response->SetStatus(mErrorCode);
            response_length = mResponseLength;
        }

        bool FilesystemResourceHandler::Skip(int64 bytes_to_skip, int64& bytes_skipped, CefRefPtr<CefResourceSkipCallback> callback)
        {
            if (!mFileStream || !mFileStream.is_open())
            {
                bytes_skipped = ERR_FAILED;
                return false;
            }

            auto pos = mFileStream.tellg();
            mFileStream.seekg(bytes_to_skip, std::ios::cur);
            bytes_skipped = mFileStream.tellg() - pos;

            return true;
        }

        bool FilesystemResourceHandler::Read(void* data_out, int bytes_to_read, int& bytes_read, CefRefPtr<CefResourceReadCallback> callback)
        {
            if (mResponseLength == 0)
            {
                bytes_read = 0;
                return false;
            }

            mFileStream.open(mFilePath, std::ios::in | std::ios::binary);
            mFileStream.unsetf(std::ios::skipws);
            mFileStream.seekg(mReadOffset, std::ios_base::beg);

            bytes_read = mFileStream.readsome((char*)data_out, bytes_to_read);
            mReadOffset = mFileStream.tellg();

            mFileStream.close();

            return bytes_read > 0;
        }

        void FilesystemResourceHandler::Cancel()
        {
            if (mFileStream && mFileStream.is_open())
                mFileStream.close();
        }
    }
}
