
#include "include/base/cef_bind.h"
#include "include/cef_app.h"
#include "include/views/cef_browser_view.h"
#include "include/views/cef_window.h"
#include "include/wrapper/cef_closure_task.h"
#include "include/wrapper/cef_helpers.h"
#include "include/cef_parser.h"

#include "client_handler.h"
#include "log.h"
#include "filesystem.h"
#include "ui/uibase.h"

#include <sstream>
#include <string>
#include <regex.h>

namespace LeagueDisplays
{
    namespace CEF
    {
        /* static */ ClientHandler* ClientHandler::gInstance;

        // Returns a data: URI with the specified contents.
        static std::string GetDataURI(const std::string& data, const std::string& mime_type) {
            return "data:" + mime_type + ";base64," +
                    CefURIEncode(CefBase64Encode(data.data(), data.size()), false).ToString();
        }

        ClientHandler::ClientHandler() : Logging::LoggerHolder{"ClientHandler"}, mIsClosing{false}
        {
            DCHECK(!gInstance);
            gInstance = this;
        }

        ClientHandler::~ClientHandler()
        {
            gInstance = nullptr;
        }

        /* static */ ClientHandler* ClientHandler::GetInstance()
        {
            return gInstance;
        }

        void ClientHandler::OnAfterCreated(CefRefPtr<CefBrowser> browser)
        {
            CEF_REQUIRE_UI_THREAD();

            // Add to the list of existing browsers.
            mBrowserList.push_back(browser);
        }

        bool ClientHandler::DoClose(CefRefPtr<CefBrowser> browser)
        {
            CEF_REQUIRE_UI_THREAD();

            // Closing the main window requires special handling. See the DoClose()
            // documentation in the CEF header for a detailed destription of this
            // process.
            if (mBrowserList.size() == 1)
            {
                // Set a flag to indicate that the window close should be allowed.
                mIsClosing = true;
            }

            // Allow the close. For windowed browsers this will result in the OS close
            // event being sent.
            return false;
        }

        void ClientHandler::OnBeforeClose(CefRefPtr<CefBrowser> browser)
        {
            CEF_REQUIRE_UI_THREAD();

            // Remove from the list of existing browsers.
            for (BrowserList::iterator bit = mBrowserList.begin(); bit != mBrowserList.end(); ++bit)
                if ((*bit)->IsSame(browser))
                {
                    mBrowserList.erase(bit);
                    break;
                }

            if (mBrowserList.empty())
            {
                // All browser windows have closed. Quit the application message loop.
                CefQuitMessageLoop();
            }
        }

        void ClientHandler::OnLoadError(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
            ErrorCode errorCode, const CefString& errorText, const CefString& failedUrl)
        {
            CEF_REQUIRE_UI_THREAD();

            // Don't display an error for downloaded files.
            if (errorCode == ERR_ABORTED)
                return;

            std::stringstream ss;
            ss << "<html>";
            ss <<   "<head>";
            ss <<     "* { background: #333; color: #aaa; }";
            ss <<   "</head>";
            ss <<   "<body>";
            ss <<     "<h2>Failed to load URL " << std::string(failedUrl) << " with error " << std::string(errorText) << " (" << errorCode << ").</h2>";
            ss <<   "</body>";
            ss << "</html>";

            frame->LoadURL(GetDataURI(ss.str(), "text/html"));
        }

        void ClientHandler::CloseAllBrowsers(bool force_close)
        {
            if (!CefCurrentlyOn(TID_UI))
            {
                // Execute on the UI thread.
                CefPostTask(TID_UI, base::Bind(&ClientHandler::CloseAllBrowsers, this, force_close));
                return;
            }

            if (mBrowserList.empty())
                return;

            for (BrowserList::const_iterator it = mBrowserList.begin(); it != mBrowserList.end(); ++it)
                (*it)->GetHost()->CloseBrowser(force_close);
        }

        bool ClientHandler::OnQuotaRequest(CefRefPtr<CefBrowser> browser, const CefString& origin_url,
            int64 new_size, CefRefPtr<CefRequestCallback> callback)
        {
            CEF_REQUIRE_IO_THREAD();

            mLogger->TraceInfo("Requested quota: %ld", new_size);

            callback->Continue(true);
            return true;
        }

        void ClientHandler::OnBeforeDownload(CefRefPtr<CefBrowser> browser, CefRefPtr<CefDownloadItem> download_item,
            const CefString& suggested_name, CefRefPtr<CefBeforeDownloadCallback> callback)
        {
            if (!download_item->IsValid())
            {
                mLogger->TraceError("download_item->IsValid() returned false!");
                return;
            }

            if (download_item->IsCanceled())
            {
                mLogger->TraceWarn("The request is already cancelled!?");
                return;
            }

            std::string suggested_name_str = suggested_name.ToString();
            auto path_str = Filesystem::gDefaultDownloadDir / suggested_name_str;

            std::string res;

            if (UI::UIManager::SaveFileDialog(path_str, suggested_name_str, res))
            {
                mLogger->TraceDebug("Saving file to %s", res.c_str());
                callback->Continue(res, false);
            }
        }

        void ClientHandler::OnDownloadUpdated(CefRefPtr<CefBrowser> browser,
            CefRefPtr<CefDownloadItem> download_item,
            CefRefPtr<CefDownloadItemCallback> callback)
        {
            int progress = download_item->GetPercentComplete();
            int is_completed = download_item->IsComplete();

            mLogger->TraceDebug("%s progress: %3d%% [completed=%-5s]",
                download_item->GetSuggestedFileName().ToString().c_str(),
                progress,
                is_completed ? "true" : "false"
            );

            if (is_completed) UI::UIManager::ShowDownloadFinishedDialog();
        }

        bool ClientHandler::RunContextMenu(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
            CefRefPtr<CefContextMenuParams> params, CefRefPtr<CefMenuModel> model,
            CefRefPtr<CefRunContextMenuCallback> callback)
        {
            callback->Cancel();
            return true;
        }
    }
}
