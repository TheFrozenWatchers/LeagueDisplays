
#ifndef LD_TELEMETRY_FILTER_H
#define LD_TELEMETRY_FILTER_H

#include "log.h"

#include "include/cef_request_context_handler.h"
#include "include/cef_parser.h"

namespace LeagueDisplays
{
    namespace CEF
    {
        class TelemetryResourceRequestBlocker : public CefResourceRequestHandler, public Logging::LoggerHolder
        {
            public:
                explicit TelemetryResourceRequestBlocker() : Logging::LoggerHolder{"TelemetryFilter"} { }

                virtual ReturnValue OnBeforeResourceLoad(
                    CefRefPtr<CefBrowser> browser,
                    CefRefPtr<CefFrame> frame,
                    CefRefPtr<CefRequest> request,
                    CefRefPtr<CefRequestCallback> callback) OVERRIDE
                {
                    callback->Cancel();
                    return RV_CANCEL;
                }
            private:
                // Include the default reference counting implementation
                IMPLEMENT_REFCOUNTING(TelemetryResourceRequestBlocker);
        };

        class TelemetryFilterContextHandler : public CefRequestContextHandler, public Logging::LoggerHolder
        {
            public:
                explicit TelemetryFilterContextHandler() : Logging::LoggerHolder{"TelemetryFilter"}
                {
                    mBlocker = new TelemetryResourceRequestBlocker;
                }

                virtual CefRefPtr<CefResourceRequestHandler> GetResourceRequestHandler(
                    CefRefPtr<CefBrowser> browser,
                    CefRefPtr<CefFrame> frame,
                    CefRefPtr<CefRequest> request,
                    bool is_navigation,
                    bool is_download,
                    const CefString& request_initiator,
                    bool& disable_default_handling) OVERRIDE
                {
                    bool fine = IsFine(request->GetURL());
                    if (fine)
                        mLogger->TraceDebug("$$3$$lFINE $$r$$8 %s", request->GetURL().ToString().c_str());
                    else
                    {
                        mLogger->TraceDebug("$$6$$lBLOCK$$r$$8 %s", request->GetURL().ToString().c_str());
                        return mBlocker;
                    }
                    return nullptr;
                }
            
            private:
                bool IsFine(const CefString& url)
                {
                    CefURLParts url_parts;
                    CefParseURL(url, url_parts);

                    CefString scheme{&url_parts.scheme};
                    CefString host{&url_parts.host};

                    // These are our hosts, so they are in completely in our control
                    if (host == "filesystem" || host == "notifications" || host == "themes")
                        return true;

                    if (host != "screensavers.riotgames.com")
                        return false;

                    if (scheme != "https" && scheme != "http")
                    {
                        mLogger->Warn("Resource is from a non-HTTP scheme: %s", url.ToString().c_str());
                        return false;
                    }

                    return true;
                }

                // Include the default reference counting implementation
                IMPLEMENT_REFCOUNTING(TelemetryFilterContextHandler);

                CefRefPtr<CefResourceRequestHandler> mBlocker;
        };
    }
}

#endif
