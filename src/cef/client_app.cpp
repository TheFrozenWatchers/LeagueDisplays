
#include "client_app.h"
#include "client_handler.h"
#include "telemetry_filter.hpp"
#include "scheme_handlers.h"
#include "crossde.h"
#include "filesystem.h"

#include "ui/uibase.h"
#include "ui/cefui.h"

#include "include/cef_browser.h"
#include "include/cef_command_line.h"
#include "include/views/cef_browser_view.h"
#include "include/views/cef_window.h"
#include "include/wrapper/cef_helpers.h"

#include "leaguedisplays_version.h"

#define LD_CEF_BIND_FUNC(o, name, handler) \
    o->SetValue(name, CefV8Value::CreateFunction(name, new JavascriptFuncWrapper(handler)), \
    V8_PROPERTY_ATTRIBUTE_NONE)

namespace LeagueDisplays
{
    namespace CEF
    {
        void ClientApp::OnBeforeCommandLineProcessing(const CefString& process_type,
              CefRefPtr<CefCommandLine> command_line)
        {
            mLogger->Debug("OnBeforeCommandLineProcessing");

            if (!command_line->HasSwitch("allow-running-insecure-content"))
                command_line->AppendSwitch("allow-running-insecure-content");

            if (!command_line->HasSwitch("autoplay-policy"))
                command_line->AppendSwitchWithValue("autoplay-policy", "no-user-gesture-required");

            if (!command_line->HasSwitch("v8-natives-passed-by-fd"))
                command_line->AppendSwitch("v8-natives-passed-by-fd");

            if (command_line->HasSwitch("ld-host-pid"))
            {
                const char* pids = command_line->GetSwitchValue("ld-host-pid").ToString().c_str();
                UI::UIManager::mHostPID = atoi(pids);
                mLogger->TraceDebug("Host pid: %s", pids);
            }

            unsigned short pid = Filesystem::PIDFileGet("cefapp.pid");

            mLogger->TraceDebug("PID from file: %d", pid);

            if (pid && pid != UI::UIManager::mHostPID && !kill(pid, 0))
            {
                mLogger->TraceError("CEF root process is already running!");
                exit(0);
                return;
            }

            Filesystem::PIDFileSet("cefapp.pid", UI::UIManager::mHostPID);
        }

        void ClientApp::OnBeforeChildProcessLaunch(CefRefPtr<CefCommandLine> command_line)
        {
            mLogger->Debug("OnBeforeChildProcessLaunch");

            command_line->AppendSwitchWithValue("ld-host-pid", std::to_string(UI::UIManager::mHostPID));
            command_line->AppendSwitchWithValue("ld-mode", "cef-app");

            auto current_command_line = CefCommandLine::GetGlobalCommandLine();

            if (current_command_line->HasSwitch("silent-logging"))
                command_line->AppendSwitch("silent-logging");

            if (current_command_line->HasSwitch("ld-block-telemetry"))
                command_line->AppendSwitch("ld-block-telemetry");

            if (current_command_line->HasSwitch("ld-log-to-file"))
                command_line->AppendSwitch("ld-log-to-file");

            if (current_command_line->HasSwitch("ld-log-level"))
                command_line->AppendSwitchWithValue("ld-log-level", current_command_line->GetSwitchValue("ld-log-level"));
        }

        void ClientApp::OnContextInitialized()
        {
            CEF_REQUIRE_UI_THREAD();
            mLogger->Debug("OnContextInitialized");

            CefRefPtr<CefCommandLine> command_line = CefCommandLine::GetGlobalCommandLine();

            // ClientHandler implements browser-level callbacks
            CefRefPtr<ClientHandler> handler{new ClientHandler};

            // Specify CEF browser settings here
            CefBrowserSettings browser_settings;
            browser_settings.local_storage              = STATE_ENABLED;
            browser_settings.databases                  = STATE_ENABLED;
            browser_settings.application_cache          = STATE_ENABLED;
            browser_settings.webgl                      = STATE_ENABLED;
            browser_settings.web_security               = STATE_DISABLED;
            browser_settings.javascript                 = STATE_ENABLED;
            browser_settings.javascript_close_windows   = STATE_ENABLED;
            browser_settings.image_loading              = STATE_ENABLED;
            browser_settings.tab_to_links               = STATE_DISABLED;

            // Make the default background dark so when the client starts it does not burn out our eyes
            browser_settings.background_color           = 0xFF222222;

            CefString(&browser_settings.default_encoding).FromASCII("UTF-8");

            CefRegisterSchemeHandlerFactory("http", "filesystem",
                new CEF::BasicSchemeHandlerFactory<CEF::FilesystemResourceHandler>);
            CefRegisterSchemeHandlerFactory("https", "filesystem",
                new CEF::BasicSchemeHandlerFactory<CEF::FilesystemResourceHandler>);

            /*CefRegisterSchemeHandlerFactory("http", "notifications",
                new CEF::BasicSchemeHandlerFactory<CEF::NotificationsResourceHandler>());
            CefRegisterSchemeHandlerFactory("https", "notifications",
                new CEF::BasicSchemeHandlerFactory<CEF::NotificationsResourceHandler>());*/

            CefRegisterSchemeHandlerFactory("http", "themes",
                new CEF::BasicSchemeHandlerFactory<CEF::ThemesResourceHandler>);
            CefRegisterSchemeHandlerFactory("https", "themes",
                new CEF::BasicSchemeHandlerFactory<CEF::ThemesResourceHandler>);

            CefRefPtr<CefRequestContext> ctx = nullptr;
            
            if (command_line->HasSwitch("ld-block-telemetry"))
            {
                ctx = CefRequestContext::CreateContext(
                    CefRequestContext::GetGlobalContext(),
                    new TelemetryFilterContextHandler
                );
            }

            std::string url;

            url = command_line->GetSwitchValue("url-override");

            if (url.empty())
                url = "https://screensavers.riotgames.com/v2/latest/index.html#/configurator?src=";

            if (command_line->HasSwitch("ld-debug-tools"))
                url = "file://" + (Filesystem::gWorkingDirectory.parent_path() / "debugtools/root.html").string();

            UI::UIManager::InitForCEF();

            // Create the BrowserView
            CefRefPtr<CefBrowserView> browser_view = CefBrowserView::CreateBrowserView(
                handler,                            // client
                url,                                // url
                browser_settings,                   // settings
                nullptr,                            // extra_info
                ctx,                                // request_context
                new UI::CEF::BrowserViewDelegate()  // delegate
            );

            // Create the Window. It will show itself after creation
            CefWindow::CreateTopLevelWindow(new UI::CEF::WindowDelegate{browser_view});

            mLogger->Info("CEF initialization completed!");
        }

        // JavaScript bindings:
        JavascriptFuncWrapper::JavascriptFuncWrapper(std::function<bool(CefRefPtr<CefV8Value>&, const CefV8ValueList&)> func)
        {
            this->mFunc = func;
            gLogger = Logging::LoggerManager::GetLogger("JavascriptHandler");
        }

        bool JavascriptFuncWrapper::Execute(const CefString& name, CefRefPtr<CefV8Value> object, const CefV8ValueList& arguments,
                    CefRefPtr<CefV8Value>& retval, CefString& exception)
        {
            return this->mFunc(retval, arguments);
        }

        /* static */ std::shared_ptr<Logging::Logger> JavascriptFuncWrapper::gLogger;

        bool _LolScreenSaverCapabilities_func_(CefRefPtr<CefV8Value>& retval, const CefV8ValueList& arguments)
        {
            int bitfield = 0;

            bitfield |= FeatureFlags::LD_FEAUTURE_FILESYSTEM;
            // bitfield |= FeatureFlags::LD_FEAUTURE_NOTIFICATIONS;
            bitfield |= FeatureFlags::LD_FEAUTURE_THEMES;
            bitfield |= FeatureFlags::LD_FEAUTURE_OFFLINE_SS;
            bitfield |= FeatureFlags::LD_FEAUTURE_MP4;
            bitfield |= FeatureFlags::LD_FEAUTURE_ASSISTANT;
            bitfield |= FeatureFlags::LD_FEAUTURE_SCREENSAVER;
            // bitfield |= FeatureFlags::LD_FEAUTURE_POWER_SETTINGS;
            bitfield |= FeatureFlags::LD_FEAUTURE_LOCKSCREEN;
            // bitfield |= FeatureFlags::LD_FEAUTURE_AUTOUPDATE;

            JavascriptFuncWrapper::gLogger->TraceDebug("LolScreenSaverCapabilities - %d", bitfield);
            retval = CefV8Value::CreateInt(bitfield);
            return true;
        }

        bool _LolScreenSaverClientVersion_func_(CefRefPtr<CefV8Value>& retval, const CefV8ValueList& arguments)
        {
            JavascriptFuncWrapper::gLogger->TraceDebug("LolScreenSaverClientVersion - %s", LD_APPVERSION);
            retval = CefV8Value::CreateString(LD_APPVERSION);
            return true;
        }

        bool _LolScreenSaverOSLanguage_func_(CefRefPtr<CefV8Value>& retval, const CefV8ValueList& arguments)
        {
            std::string tmp = std::locale("").name();
            size_t scpos = tmp.find(';');

            if (scpos < tmp.length())
                tmp = tmp.substr(0, scpos);

            size_t eqpos = tmp.find('=');

            if (eqpos < tmp.length())
                tmp = tmp.substr(eqpos + 1);

            JavascriptFuncWrapper::gLogger->TraceDebug("LolScreenSaverOSLanguage - %s", tmp.c_str());
            retval = CefV8Value::CreateString(tmp);
            return true;
        }

        bool _LolScreenSaverCloseConfigurator_func_(CefRefPtr<CefV8Value>& retval, const CefV8ValueList& arguments)
        {
            JavascriptFuncWrapper::gLogger->TraceDebug("LolScreenSaverCloseConfigurator");
            UI::UIManager::CloseGracefully();
            return true;
        }

        bool _LolScreenSaverOpenUrl_func_(CefRefPtr<CefV8Value>& retval, const CefV8ValueList& arguments)
        {
            if (arguments.size() < 1)
                return false;

            std::string url = arguments[0]->GetStringValue().ToString();
            JavascriptFuncWrapper::gLogger->TraceDebug("LolScreenSaverOpenUrl - %s", url.c_str());
            DesktopEnvApi::OpenUrlInDefaultBrowser(url);
            return true;
        }

        bool _addEventListener_func_(CefRefPtr<CefV8Value>& retval, const CefV8ValueList& arguments)
        {
            std::string event = arguments[0]->GetStringValue().ToString();
            JavascriptFuncWrapper::gLogger->TraceDebug("addEventListener - %s", event.c_str());

            if (event == "noupdate")
            {
                CefV8ValueList args;
                arguments[1]->ExecuteFunction(nullptr, args);
            }

            return true;
        }

        bool _removeEventListener_func_(CefRefPtr<CefV8Value>& retval, const CefV8ValueList& arguments)
        {
            std::string event = arguments[0]->GetStringValue().ToString();
            JavascriptFuncWrapper::gLogger->TraceDebug("removeEventListener - %s", event.c_str());
            return true;
        }

        void ClientApp::OnContextCreated(CefRefPtr<CefBrowser> browser, CefRefPtr<CefFrame> frame,
            CefRefPtr<CefV8Context> context)
        {
            CefRefPtr<CefV8Value> obj = context->GetGlobal();
            CefRefPtr<CefV8Value> applicationCache = CefV8Value::CreateObject(nullptr, nullptr);

            mLogger->TraceDebug("Binding functions...");

            LD_CEF_BIND_FUNC(obj, "LolScreenSaverCapabilities",       _LolScreenSaverCapabilities_func_       );
            LD_CEF_BIND_FUNC(obj, "LolScreenSaverClientVersion",      _LolScreenSaverClientVersion_func_      );
            LD_CEF_BIND_FUNC(obj, "LolScreenSaverOSLanguage",         _LolScreenSaverOSLanguage_func_         );
            LD_CEF_BIND_FUNC(obj, "LolScreenSaverCloseConfigurator",  _LolScreenSaverCloseConfigurator_func_  );
            LD_CEF_BIND_FUNC(obj, "LolScreenSaverOpenUrl",            _LolScreenSaverOpenUrl_func_            );

            LD_CEF_BIND_FUNC(applicationCache, "addEventListener",    _addEventListener_func_    );
            LD_CEF_BIND_FUNC(applicationCache, "removeEventListener", _removeEventListener_func_ );

            obj->SetValue("applicationCache", applicationCache, V8_PROPERTY_ATTRIBUTE_NONE);
        }
    }
}
