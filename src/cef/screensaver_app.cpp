
#include "screensaver_app.h"
#include "client_handler.h"

#include <string>
#include <X11/Xlib.h>
#include <gdk/gdkx.h>
#include <gtk/gtk.h>

#include "ui/uibase.h"
#include "scheme_handlers.h"
#include "log.h"
#include "filesystem.h"

#include "include/cef_browser.h"
#include "include/cef_command_line.h"
#include "include/views/cef_browser_view.h"
#include "include/views/cef_window.h"
#include "include/wrapper/cef_helpers.h"

namespace LeagueDisplays
{
    namespace CEF
    {
        void ScreensaverApp::OnBeforeCommandLineProcessing(const CefString& process_type,
              CefRefPtr<CefCommandLine> command_line)
        {

            if (!command_line->HasSwitch("allow-running-insecure-content"))
                command_line->AppendSwitch("allow-running-insecure-content");

            if (!command_line->HasSwitch("autoplay-policy"))
                command_line->AppendSwitchWithValue("autoplay-policy", "no-user-gesture-required");

            if (!command_line->HasSwitch("v8-natives-passed-by-fd"))
                command_line->AppendSwitch("v8-natives-passed-by-fd");

            if (command_line->HasSwitch("ld-host-pid"))
            {
                const char *pids = command_line->GetSwitchValue("ld-host-pid").ToString().c_str();
                UI::UIManager::mHostPID = atoi(pids);
                mLogger->TraceDebug("Host pid: %s", pids);
            }

            unsigned short pid = Filesystem::PIDFileGet("screensaver.pid");
            mLogger->TraceDebug("PID from file: %d", pid);
            Filesystem::PIDFileSet("screensaver.pid", UI::UIManager::mHostPID);
        }

        void ScreensaverApp::OnBeforeChildProcessLaunch(CefRefPtr<CefCommandLine> command_line)
        {
            command_line->AppendSwitchWithValue("ld-host-pid", std::to_string(UI::UIManager::mHostPID));
            command_line->AppendSwitchWithValue("ld-mode", "screensaver");

            auto current_command_line = CefCommandLine::GetGlobalCommandLine();

            if (current_command_line->HasSwitch("silent-logging"))
                command_line->AppendSwitch("silent-logging");

            if (current_command_line->HasSwitch("ld-block-telemetry"))
                command_line->AppendSwitch("ld-block-telemetry");

            if (current_command_line->HasSwitch("ld-log-to-file"))
                command_line->AppendSwitch("ld-log-to-file");

            if (current_command_line->HasSwitch("ld-log-level"))
                command_line->AppendSwitchWithValue("ld-log-level", current_command_line->GetSwitchValue("ld-log-level"));

            if (current_command_line->HasSwitch("ld-scr-root-process"))
                command_line->AppendSwitchWithValue("ld-scr-root-process", current_command_line->GetSwitchValue("ld-scr-root-process"));
            else
                command_line->AppendSwitchWithValue("ld-scr-root-process", std::to_string(getpid()));
        }
    }
}
