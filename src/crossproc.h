
#ifndef LD_CROSSPROC_H
#define LD_CROSSPROC_H

#include "log.h"

#include <pthread.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <limits.h>

#include <sys/mman.h>

namespace LeagueDisplays
{
    template<typename T, size_t extra_alloc = 0>
    class CrossProcessObject : public Logging::LoggerHolder
    {
        public:
            CrossProcessObject(const char* name) : Logging::LoggerHolder{"CrossProcessObject"}
            {
                errno = 0;

                mIsCreated = false;
                mName = name;

                mSharedMemoryObject = shm_open(name, O_RDWR, 0660);
                bool nowCreated = false;

                if (errno == ENOENT)
                {
                    mLogger->TraceDebug("Looks like the object does not exist yet");
                    mSharedMemoryObject = shm_open(name, O_RDWR | O_CREAT, 0660);
                    nowCreated = true;
                }

                if (errno != 0 && mSharedMemoryObject == -1)
                {
                    mLogger->TraceError("Could not create shared memory error=%d", errno);
                    return;
                }

                mIsCreated = true;

                if (nowCreated)
                    mLogger->Info("Created shared memory space called %s", name);
                else
                    mLogger->Info("Opened shared memory space called %s", name);

                if (mSharedMemoryObject == -1)
                {
                    mLogger->TraceError("shm_open");
                    return;
                }

                if (ftruncate(mSharedMemoryObject, sizeof(T) + extra_alloc + 1) != 0)
                {
                    mLogger->TraceError("ftruncate");
                    return;
                }

                void* addr = mmap(NULL, sizeof(T) + extra_alloc + 1, PROT_READ | PROT_WRITE,
                    MAP_SHARED, mSharedMemoryObject, 0);

                if (addr == MAP_FAILED)
                {
                    mLogger->TraceError("mmap");
                    return;
                }

                if (nowCreated)
                {
                    mLogger->TraceDebug("filling object with zeros [p=%p,n=%d]", addr, sizeof(T) + extra_alloc + 1);
                    memset(addr, 0, sizeof(T) + extra_alloc + 1);
                }

                mObject = (T*)addr;
                mIsAssigned = ((bool*)addr) + sizeof(T) + extra_alloc;
            }

            void Destroy()
            {
                mLogger->TraceDebug("Destroying shared memory space called %s\n", mName);
                mIsCreated = false;

                if (munmap((void*)mObject, sizeof(T) + extra_alloc + 1))
                {
                    mLogger->TraceError("munmap");
                    return;
                }

                mObject = NULL;
                if (close(mSharedMemoryObject))
                {
                    mLogger->TraceError("close");
                    return;
                }

                mSharedMemoryObject = 0;
                if (shm_unlink(mName))
                {
                    mLogger->TraceError("shm_unlink");
                    return;
                }
            }

            const bool IsValid() const noexcept
            {
                return mIsCreated;
            }

            const bool AlreadyAssigned() const noexcept
            {
                return *mIsAssigned;
            }

            void Assign(T* obj)
            {
                assert(mIsCreated);
                memcpy(mObject, obj, sizeof(T));
                *mIsAssigned = true;
            }

            T *Get()
            {
                assert(mIsCreated);
                return mObject;
            }
        private:
            bool                             mIsCreated;
            bool*                            mIsAssigned;
            const char*                      mName;
            int                              mSharedMemoryObject;
            T*                               mObject;
    };

    class CrossProcessMutex : public Logging::LoggerHolder
    {
        public:
            CrossProcessMutex(const char* name);
            ~CrossProcessMutex();
            void Lock();
            bool TryLock();
            void Unlock();
            void Destroy();

        private:
            CrossProcessObject<pthread_mutex_t>*    mMutexPtr;
            const char*                             mName;
    };
}

#endif
