
#include "log.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <regex>
#include <thread>
#include <fstream>
#include <sstream>
#include <iomanip>

namespace LeagueDisplays
{
    namespace Logging
    {
        /* static */ std::unique_ptr<LoggerManager> LoggerManager::gInstance;
        /* static */ std::shared_ptr<DummyLogger> LoggerManager::gDummyLogger;

        /* static */ void LoggerManager::Initialize(bool log2file) {
            gInstance = std::unique_ptr<LoggerManager>{new LoggerManager{log2file}};
        }

        LogOutputWriter::LogOutputWriter(std::ostream& console, streamptr file)
            : mConsoleOutput{console}, mFileOutput{std::move(file)}, mRunningAsync{false}
        {
        }

        LogOutputWriter::~LogOutputWriter()
        {
            mRun = false;

            if (mPollingThread && mPollingThread->joinable())
                mPollingThread->join();

            while (Poll());
            Flush();
        }

        void LogOutputWriter::Output(std::string what)
        {
            mQueueMutex.lock();
            mLogQueue.push_back(std::move(what));
            mQueueMutex.unlock();

            while (!mRunningAsync && Poll());
        }

        void LogOutputWriter::Flush()
        {
            mOutputMutex.lock();

            if (mConsoleOutput)
                mConsoleOutput.flush();

            if (mFileOutput && *mFileOutput)
                mFileOutput->flush();

            mOutputMutex.unlock();
        }

        void LogOutputWriter::FlushQueue()
        {
            if (mRunningAsync)
                while (!mLogQueue.empty());
            else
                while (Poll());

            Flush();
        }

        void LogOutputWriter::GoAsync() noexcept
        {
            if (mRunningAsync)
                return;

            mRunningAsync = true;

            // Output("<<< NOW RUNNING WITH ASYNC OUTPUT >>>\n");

            mPollingThread = std::make_unique<std::thread>([this]() -> void {
                bool didAnything;
                mRun = true;

                while (mRun)
                {
                    didAnything = false;

                    while (Poll())
                        didAnything = true;

                    if (didAnything)
                        Flush();
                    else
                        usleep(1000);
                }

                mRunningAsync = false;
            });
        }

        bool LogOutputWriter::Poll()
        {
            if (mRunningAsync && mPollingThread && std::this_thread::get_id() != mPollingThread->get_id())
            {
                fprintf(stderr, "Called logger Poll from a different thread");
                abort();
            }

            mQueueMutex.lock();

            if (mLogQueue.size() == 0)
            {
                mQueueMutex.unlock();
                return false;
            }

            std::string tmp = std::move(mLogQueue.front());
            mLogQueue.pop_front();

            mQueueMutex.unlock();

            mOutputMutex.lock();

            if (mConsoleOutput)
                OutputWithFormatting(mConsoleOutput, tmp);

            if (mFileOutput && *mFileOutput)
                OutputWithoutFormatting(*mFileOutput, tmp);

            mOutputMutex.unlock();

            return true;
        }

        /* static */ void LogOutputWriter::OutputWithFormatting(std::ostream& os, std::string input)
        {
            os << "\e[39m\e[0m";

            for (size_t pos = 0; pos != input.length(); pos++)
            {
                if (input.length() - pos >= 3 && input.compare(pos, 2, "$$") == 0)
                {
                    pos += 2; // remove the $$ from the begining

                    switch (input[pos]) {
                        // COLORS:
                        case '0':
                            os << "\e[30m";
                            break;
                        case '1':
                            os << "\e[34m";
                            break;
                        case '2':
                            os << "\e[32m";
                            break;
                        case '3':
                            os << "\e[36m";
                            break;
                        case '4':
                            os << "\e[31m";
                            break;
                        case '5':
                            os << "\e[35m";
                            break;
                        case '6':
                            os << "\e[33m";
                            break;
                        case '7':
                            os << "\e[37m";
                            break;
                        case '8':
                            os << "\e[90m";
                            break;
                        case '9':
                            os << "\e[94m";
                            break;
                        case 'a':
                            os << "\e[92m";
                            break;
                        case 'b':
                            os << "\e[96m";
                            break;
                        case 'c':
                            os << "\e[91m";
                            break;
                        case 'd':
                            os << "\e[95m";
                            break;
                        case 'e':
                            os << "\e[93m";
                            break;
                        case 'f':
                            os << "\e[97m";
                            break;

                        // FORMATS:
                        case 'k':
                            os << "\e[2m";
                            break;
                        case 'l':
                            os << "\e[1m";
                            break;
                        case 'm':
                            os << "\e[5m";
                            break;
                        case 'n':
                            os << "\e[4m";
                            break;
                        case 'o':
                            os << "\e[7m";
                            break;
                        case 'r':
                            os << "\e[39m\e[0m";
                            break;
                        default:
                            os << input[pos];
                            break;
                    }
                }
                else os << input[pos];
            }

            os << "\e[39m\e[0m";
        }

        /* static */ void LogOutputWriter::OutputWithoutFormatting(std::ostream& os, std::string input)
        {
            for (size_t pos = 0; pos != input.length(); pos++)
            {
                if (input.length() - pos >= 3 && input.compare(pos, 2, "$$") == 0)
                {
                    pos += 2; // remove the $$ from the begining

                    switch (input[pos]) {
                        // COLORS:
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                        case 'a':
                        case 'b':
                        case 'c':
                        case 'd':
                        case 'e':
                        case 'f':

                        // FORMATS:
                        case 'k':
                        case 'l':
                        case 'm':
                        case 'n':
                        case 'o':
                        case 'r':
                            break;
                        default:
                            os << input[pos];
                            break;
                    }
                }
                else os << input[pos];
            }
        }


        Logger::Logger(std::shared_ptr<LogOutputWriter> writer, std::string name)
            : mWriter{writer}, mName{std::move(name)} { }

        void Logger::LogRaw(const char *trace, LogLevel level, std::string message)
        {
            if (!LoggerManager::Instance().IsLevelEnabled(level))
                return;

            const char *levelstr = "???";

            switch (level) {
                case LL_DEBUG:
                    levelstr = "debug";
                    message = "$$8" + message;
                    break;
                case LL_INFO:
                    levelstr = "info";
                    message = "$$a" + message;
                    break;
                case LL_FIXME:
                    levelstr = "fixme";
                    message = "$$d" + message;
                    break;
                case LL_WARN:
                    levelstr = "warn";
                    message = "$$e" + message;
                    break;
                case LL_ERROR:
                    levelstr = "error";
                    message = "$$c" + message;
                    break;
            }

            // C format string for reference: "[%04x] (%-20s) %6s:%-20s: %s\n"

            std::stringstream ss;
            ss << '[' << std::setfill('0') << std::setw(4) << std::hex << getpid() << "] ";
            ss << '(' << std::setfill(' ') << std::setw(20) << mName << ") ";
            ss << std::setw(6) << levelstr;

            if (trace)
                ss << ':' << std::setw(20) << trace;

            ss << ": " << message << std::endl;

            mWriter->Output(ss.str());
        }

        void Logger::LogOutputRaw(LogLevel level, std::string message)
        {
            if (!LoggerManager::Instance().IsLevelEnabled(level))
                return;

            mWriter->Output(message);
        }


        LoggerManager::LoggerManager(bool log2file) : mMinimumLevel{LogLevel::LL_DEFAULT}
        {
            if (log2file)
            {
                struct stat st = {0};

                if (stat("logs/", &st) == -1)
                    mkdir("logs/", 0700);

                time_t rawtime;
                struct tm *timeinfo;

                time(&rawtime);
                timeinfo = localtime(&rawtime);

                char logfname[256];
                snprintf(
                    logfname,
                    256,
                    "logs/LeagueDisplays_P%04x_%04d-%02d-%02dT%02d-%02d-%02d.log",
                    getpid(),
                    timeinfo->tm_year + 1900,
                    timeinfo->tm_mon + 1,
                    timeinfo->tm_mday,
                    timeinfo->tm_hour,
                    timeinfo->tm_min,
                    timeinfo->tm_sec
                );

                mGlobalLogWriter = std::make_shared<LogOutputWriter>(std::cout, std::make_unique<std::ofstream>(logfname));
            }
            else
            {
                mGlobalLogWriter = std::make_shared<LogOutputWriter>(std::cout, nullptr);
            }
        }

        /* static */ std::shared_ptr<Logger> LoggerManager::GetLogger(std::string name)
        {
            // In case the logger managger is not initialized (yet), return a dummy logger
            if (!gInstance)
            {
                if (!gDummyLogger)
                    gDummyLogger = std::make_shared<DummyLogger>();

                return gDummyLogger;
            }

            std::lock_guard<std::mutex>{gInstance->mLoggersMutex};

            if (gInstance->mLoggers.find(name) != gInstance->mLoggers.end())
                return gInstance->mLoggers[name];

            auto res = std::make_shared<Logger>(gInstance->mGlobalLogWriter, name);
            gInstance->mLoggers.insert({std::move(name), res});
            return res;
        }

        LogLevel LoggerManager::GetMinimumLogLevel() const noexcept
        {
            return mMinimumLevel;
        }

        void LoggerManager::SetMinimumLogLevel(LogLevel level) noexcept
        {
            mMinimumLevel = level;
        }

        bool LoggerManager::IsLevelEnabled(LogLevel level) const noexcept
        {
            return mMinimumLevel <= level;
        }

        void LoggerManager::EnableAsyncLogging() noexcept
        {
            mGlobalLogWriter->GoAsync();
        }
    }
}
