
#ifndef LD_APP_CONFIG_H
#define LD_APP_CONFIG_H

#include <pthread.h>
#include <vector>
#include <string>

#include "crossproc.h"
#include "log.h"

// X11 defines 'Bool' so we need to undef it
// before including rapidjson stuff
#ifdef Bool
#undef Bool
#include "include/rapidjson/document.h"
#define Bool int
#else
#include "include/rapidjson/document.h"
#endif // Bool

#ifndef ModifyOptionS
#define ModifyOptionS(name, value, type) ModifyOption(Misc::fnvHashConst(name), value, type);
#endif // ModifyOptionS

namespace LeagueDisplays
{
    enum class ConfigType
    {
        GENERAL,
        WALLPAPER,
        SCREENSAVER
    };

    struct ld_settings_t
    {
        // General settings:
        int64_t     firstTimeConfig;
        int64_t     lifetimeSessions;
        int64_t     lifetimeSaves;
        int64_t     wp_imageDuration;
        int64_t     ss_videoDuration;
        bool        ss_mirrorDisplay;
        int64_t     agreeLicenseTimestamp;
        int64_t     openConfigTimestamp;
        int64_t     newestContentTimestamp;
        uint32_t    locale;
        bool        crop_images;
        int64_t     savePlaylistTimestamp;

        // Wallpaper settings:
        int64_t     wallpaper_rotate_frequency;

        // Screensaver settings:
        int64_t     screensaver_timeout;

        // Cross-process reload requests
        bool        mReloadConfig;
        bool        mReloadPlaylist;
    };

    class AppConfig
    {
        public:
            class AcquiredAppConfig
            {
                public:
                    AcquiredAppConfig() : mConfig{nullptr} {}
                    AcquiredAppConfig(AppConfig* cfg) : mConfig{cfg} {}
                    AcquiredAppConfig(const AcquiredAppConfig&) = delete;
                    AcquiredAppConfig(AcquiredAppConfig&& other) : mConfig{other.mConfig} {
                        other.mConfig = nullptr;
                    }

                    AcquiredAppConfig& operator==(const AcquiredAppConfig&) = delete;
                    AcquiredAppConfig& operator==(AcquiredAppConfig&& other) {
                        mConfig = other.mConfig;
                        other.mConfig = nullptr;
                    }

                    ~AcquiredAppConfig() { Release(); }

                    void Release()
                    {
                        if (mConfig)
                        {
                            mConfig->Release();
                            mConfig = nullptr;
                        }
                    }

                    AppConfig* operator->() {
                        return mConfig;
                    }
                private:
                    AppConfig* mConfig;
            };

            static AcquiredAppConfig Acquire();

            void LoadSettings();
            void LoadPlaylist();
            bool ModifyOption(const std::string& fnv_name, const rapidjson::Value& value, const ConfigType type);
            bool ModifyOption(const unsigned int fnv_name, const rapidjson::Value& value, const ConfigType type);

            void ReloadIfRequested();

            std::vector<std::string>& BackgroundPlaylist();
            std::vector<std::string>& ScreensaverPlaylist();
            ld_settings_t *Settings();
            bool BackgroundPlaylistChanged();
            bool ScreensaverPlaylistChanged();
        private:
            AppConfig();

            void Release();

            static AppConfig*                           mInstance;

            std::vector<std::string>                    mBackgroundPlaylist;
            std::vector<std::string>                    mScreensaverPlaylist;

            CrossProcessMutex*                          mConfigMutex;
            CrossProcessObject<ld_settings_t>*          mConfig;

            static Logging::LoggerPtr                   gLogger;

            bool                                        mBackgroundPlaylistChanged;
            bool                                        mScreensaverPlaylistChanged;
    };
}

#endif
