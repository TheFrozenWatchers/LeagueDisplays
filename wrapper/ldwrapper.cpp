#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char** argv) {
    char buf[PATH_MAX];
    char *res = realpath("/proc/self/exe", buf);

    if (res) {
        printf("We are %s\n", buf);

        int len = strlen(buf);
        while (len > 0 && buf[len - 1] != '/')
            buf[--len] = 0;

        printf("Working directory should be %s\n", buf);
        int rc = chdir(buf);
        if (rc) {
            perror("chdir");
            exit(EXIT_FAILURE);
        }

        const char* path = strcat(buf, "cefld");
        printf("Passing execution towards %s\n", path);
        return execv(path, argv);
    } else {
        perror("realpath");
        exit(EXIT_FAILURE);
    }

    return 0;
}
