#!/bin/python3

from urllib.request import urlopen
import json, platform, sys

DEBUG=False

output = urlopen('https://cef-builds.spotifycdn.com/index.json')
data = json.load(output)

systype = platform.system()
sysarch = platform.machine()

if DEBUG: print("System:", systype, sysarch)
# TODO: check architecture and os

data = data["linux64"]["versions"][0]

if DEBUG: print("Found that the latest CEF version is %s" % data["cef_version"])

for x in data["files"]:
    if x["type"] == "minimal":
        print("https://cef-builds.spotifycdn.com/%s" % x["name"])
        exit(0)

if DEBUG: print("Falling back to standard version of CEF")

for x in data["files"]:
    if x["type"] == "standard":
        print("https://cef-builds.spotifycdn.com/%s" % x["name"])
        exit(0)

print("Cannot find file for the latest CEF version :(", file=sys.stderr)
exit(1)
